local opcodes = require('assembler.opcodes')
local Object = require('assembler.object')
local Generation = {}

function Generation.new()
    return {
        numConstants = 0,
        constants = {},
        numStrings = 0,
        strings = {},
        instructions = {},
        labels = {},
        functions = {},
        functionsById = {}
    }
end

local function registerToNumber(r)
    if string.sub(r, 1, 1) ~= 'r' then
        error("Register badly formatted, got " .. r)
    end

    return tonumber(string.sub(r, 2))
end

local function generateA(instruction)
    local opcode, dest, src1, src2 = table.unpack(instruction)
    if opcode.generate then
        dest, src1, src2 = opcode.generate(dest, src1, src2)
    end

    return string.pack("<bbbb",
        opcode.opcode,
        registerToNumber(dest),
        registerToNumber(src1),
        registerToNumber(src2))
end

local function generateB(instruction, i, labels, constants, strings, functions, signed)
    local opcode, dest, data = table.unpack(instruction)
    if opcode.generate then
        dest, data = opcode.generate(dest, data, i, labels, constants, strings, functions)
    end

    return string.pack(signed and "<bbi2" or "<bbI2",
        opcode.opcode,
        registerToNumber(dest),
        tonumber(data))
end

local function generateInstruction(instruction, i, labels, constants, strings, functions)
    local opcodeData = instruction[1]
    if opcodeData.iType == 'A' then
        return generateA(instruction, i, labels, constants)
    elseif opcodeData.iType == 'BS' then
        return generateB(instruction, i, labels, constants, strings, functions, true)
    elseif opcodeData.iType == 'BU' then
        return generateB(instruction, i, labels, constants, strings, functions, false)
    else
        error('Unknown instruction type ' .. tostring(opcodeData.iType))
    end
end

function Generation:visit(opcode, dest, src1, src2)
    local opcodeData = opcodes[opcode]
    if opcodeData == nil then
        error("Invalid opcode " .. opcode)
    end

    self.instructions[#self.instructions + 1]
        = {opcodeData, dest, src1, src2}
end

function Generation:visitLabel(name)
    self.labels[name] = #self.instructions + 1
end

function Generation:visitConstant(name, value, t)
    if self.constants[name] then
        error("Multiple definitions of constant " .. name .. " found")
    end
    self.constants[name] = {
        value = value,
        t = t,
        name = name,
        id = self.numConstants
    }
    self.numConstants = self.numConstants + 1
end

function Generation:visitString(name, value)
    if self.strings[name] then
        error("Multiple definitions of string " .. name .. " found")
    end

    self.strings[name] = {
        value = value,
        id = self.numStrings
    }
    self.numStrings = self.numStrings + 1
end

function Generation:visitFunction(name, numArgs, numUpvals)
    self.functionsById[#self.functionsById + 1] = {
        name = name,
        numArgs = numArgs,
        numUpvals = numUpvals,
        location = #self.instructions
    }
    self.functions[name] = #self.functionsById - 1
end

function Generation:generateNoHeader()
    local bytecode = {}
    for i, instruction in ipairs(self.instructions) do
        bytecode[#bytecode + 1] = generateInstruction(instruction, i, self.labels, self.constants, self.strings, self.functions)
    end

    return table.concat(bytecode, '')
end

local function generateStringData(self)
    local strings = {}
    for name, str in pairs(self.strings) do
        strings[str.id + 1] = name
    end

    local header = {}
    local currentEnd = 0
    local stringData = {}
    for _, name in ipairs(strings) do
        currentEnd = currentEnd + self.strings[name].value:len()
        stringData[#stringData + 1] = self.strings[name].value
        header[#header+1] = string.pack('<I4', currentEnd)
    end
    if #strings % 2 == 1 then
        header[#header+1] = string.pack('<I4', 0)
    end

    local finalResult = table.concat(header, '') .. table.concat(stringData, '')
    if finalResult:len() % 8 ~= 0 then
        finalResult = finalResult
            .. string.pack('<I' .. (8 - (finalResult:len() % 8)), 0)
    end

    return finalResult
end

local function generateFunctionData(self)
    local result = {}
    for i, f in ipairs(self.functionsById) do
        result[i] = string.pack("<BBi2I4", f.numUpvals, f.numArgs, 0, f.location)
    end

    return table.concat(result)
end

function Generation:generate()
    local bytecode = {}
    bytecode[1] = string.pack("<I4I4I8", 0x4757494c, 1, self.numConstants)

    -- Generate constant section
    for _, constant in pairs(self.constants) do
        bytecode[constant.id + 2] = Object.generate(constant.value, constant.t)
    end

    -- Generate string section
    bytecode[#bytecode + 1] = string.pack("<I8", self.numStrings)
    bytecode[#bytecode + 1] = generateStringData(self)

    -- Generate function section
    bytecode[#bytecode + 1] = string.pack("<I8", #self.functionsById)
    bytecode[#bytecode + 1] = generateFunctionData(self)

    return table.concat(bytecode) .. self:generateNoHeader()
end

local Generation_mt = {__index = Generation}
return function ()
    return setmetatable(Generation.new(), Generation_mt)
end
