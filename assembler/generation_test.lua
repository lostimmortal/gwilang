#!/usr/bin/env lua

local Generation = require('assembler.generation')
local test = require('assembler.test_helper')

local g
test.setup(function ()
    g = Generation()
end)

test('A type generation', function ()
    g:visit('add', 'r1', 'r2', 'r3')
    local code = g:generateNoHeader()
    local opcode, reg1, reg2, reg3 = string.unpack("<bbbb", code)
    assert(opcode == 2)
    assert(reg1 == 1)
    assert(reg2 == 2)
    assert(reg3 == 3)
end)

test('B type generation', function ()
    g:visit('loadshort', 'r1', '32767')
    local code = g:generateNoHeader()
    local opcode, reg1, data = string.unpack("<bbi2", code)
    assert(opcode == 1)
    assert(reg1 == 1)
    assert(data == 32767)
end)

test('nop generation', function ()
    g:visit('nop')
    local code = g:generateNoHeader()
    local opcode, rest = string.unpack("<bi3", code)
    assert(opcode == 0)
    assert(rest == 0)
end)

test('labels', function ()
    g:visitLabel('above')
    g:visit('jmp', 'below')
    g:visit('jmp', 'above')
    g:visitLabel('below')
    g:visit('nop')

    local code = g:generateNoHeader()
    local _, _, location1,
        _, _, location2 = string.unpack("<bbi2bbi2", code)

    assert(location1 == 2)
    assert(location2 == -1)
end)

test('can output floating point constants', function ()
    g:visitConstant('someConstant', '32.2', 'number')
    local code = g:generate()
    local _, num, constant = string.unpack("<i8I8d", code)
    assert(num == 1)

    assert(constant == 32.2)
end)

test('can output integers', function ()
    g:visitConstant('someConstant', '392', 'int32')
    local code = g:generate()
    local _, num, constant, _ = string.unpack("<i8I8i4i4", code)
    assert(num == 1)
    assert(constant == 392)
end)

test('loadk', function ()
    g:visitConstant('constant', '493', 'int32')
    g:visit('loadk', 'r0', 'constant')

    local code = g:generateNoHeader()
    local opcode, reg, data = string.unpack("<bbI2", code)
    assert(opcode == 7)
    assert(reg == 0)
    assert(data == 0)
end)

test('loadstr', function ()
    g:visitString('string', 'hello')
    g:visit('loadstr', 'r0', 'string')

    local code = g:generateNoHeader()
    local opcode, reg, data = string.unpack("<bbI2", code)
    assert(opcode == 8)
    assert(reg == 0)
    assert(data == 0)
end)

test('error if multiple definitions', function ()
    local ok = pcall(function ()
        g:visitConstant('constant', '493', 'int32')
        g:visitConstant('constant', '493', 'int32')
    end)

    assert(not ok)
end)

test('generates correct header', function ()
    g:visit('nop')
    local code = g:generate()
    local header, version = string.unpack('<I4I4', code)
    assert(header == 0x4757494c)
    assert(version == 1)
end)

test('generates string data', function ()
    g:visitString('string', "some string")
    g:visitString('empty', "")

    local code = g:generate()
    local _, numStrings, stringPos1, stringPos2, stringData =
        string.unpack('<c16I8i4i4c11', code)
    assert(numStrings == 2)
    assert(stringPos1 == 11)
    assert(stringPos2 == 11)
    assert(stringData == 'some string')
end)

test('errors when setting the same string twice', function ()
    local ok = pcall(function ()
        g:visitString('string', "some string")
        g:visitString('string', "")
    end)

    assert(not ok)
end)

test('generates function headers', function ()
    g:visitFunction('testfn', 3, 5)
    g:visit('add', 'r0', 'r1', 'r2')

    local code = g:generate()
    local _, numFunctions, numUpvals, numArgs, _, bytecodeLocation =
        string.unpack('<c24I8BBc2I4', code)

    assert(numFunctions == 1)
    assert(numUpvals == 5)
    assert(numArgs == 3)
    assert(bytecodeLocation == 0)
end)

test('loadfn', function ()
    g:visitFunction('testfn', 3, 5)
    g:visit('ret', 'r0')
    g:visitFunction('top', 0, 0)
    g:visit('loadfn', 'r0', 'testfn')

    local code = g:generateNoHeader()
    local _, opcode, _, fnId = string.unpack("<c4BBI2", code)
    assert(opcode == 27)
    assert(fnId == 0)
end)

test.run()
