local Object = {}

local function objectHeader(typenum)
    return 0x7ff00000 | (typenum << 16) 
end

local function generateNumber(n)
    return string.pack("<d", n)
end

local function generateInteger(n)
    local header = objectHeader(4)
    return string.pack("<i4I4", n, header)
end

function Object.generate(obj, objtype)
    if objtype == 'number' then
        return generateNumber(obj)
    elseif objtype == 'int32' then
        return generateInteger(obj)
    end

    error('Unsupported type ' .. objtype)
end

return Object
