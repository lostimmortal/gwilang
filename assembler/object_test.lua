#!/usr/bin/env lua

local Object = require('assembler.object')
local test = require('assembler.test_helper')

test('can create numbers', function ()
    local o = Object.generate(3.4, 'number')
    assert(string.unpack("<d", o) == 3.4)
end)

test('can create ints', function ()
    local o = Object.generate(123123, 'int32')
    local int, header = string.unpack("<i4I4", o)
    assert(int == 123123)
    assert(header == 0x7ff40000 | (4 << 16))
end)

test('throws if given an unsupported type', function ()
    local ok = pcall(function ()
        Object.generate('asoenth', 'unsupported')
    end)

    assert(not ok)
end)

test.run()
