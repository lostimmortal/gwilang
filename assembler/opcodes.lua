local function jumpGenerate(reg, dest, i, labels)
    local l = labels[dest]
    if not l then
        error('Could not find label ' .. tostring(dest))
    end
    return reg, l - i
end
--[[

Opcodes stored here have the following format:

name = {
    iType = < 'A' | 'BU' | 'BS' >,
    opcode = < opcode number >,
    [ generate = function (dest, <src1, src2 | data>) ]
}

The generate function is there for special opcodes which
do not follow the standard conventions. For example,
nop which is neither of type a or b will assert that it has
no arguments passed to it.

The generate function should transform the 2/3 values
(depending on the instruction type) which will be filled
into the instruction by the generator.

--]]
return {
    nop = {
        iType = 'A',
        opcode = 0,
        generate = function ()
            return 'r0', 'r0', 'r0'
        end
    },

    loadshort = {
        iType = 'BS',
        opcode = 1
    },

    add = {
        iType = 'A',
        opcode = 2
    },

    jmp = {
        iType = 'BS',
        opcode = 3,
        generate = function (dest, _, i, labels)
            return jumpGenerate('r0', dest, i, labels)
        end
    },

    addk = {
        iType = 'A',
        opcode = 4,
        generate = function (dest, src1, constant)
            return dest, src1, 'r' .. constant
        end
    },

    loadbool = {
        iType = 'BS',
        opcode = 5
    },

    jt = {
        iType = 'BS',
        opcode = 6,
        generate = jumpGenerate
    },

    loadk = {
        iType = 'BU',
        opcode = 7,
        generate = function (dest, data, _, _, constants)
            return dest, constants[data].id
        end
    },

    loadstr = {
        iType = 'BU',
        opcode = 8,
        generate = function (dest, data, _, _, _, strings)
            return dest, strings[data].id
        end
    },

    newdict = {
        iType = 'BU',
        opcode = 9
    },

    dictput = {
        iType = 'A',
        opcode = 10
    },

    dictget = {
        iType = 'A',
        opcode = 11
    },

    call = {
        iType = 'A',
        opcode = 12,
        generate = function (dest, src1, src2)
            return dest, src1, 'r' .. src2
        end
    },

    loadg = {
        iType = 'A',
        opcode = 13,
        generate = function (dest, src1)
            return dest, src1, 'r0'
        end
    },

    lt = {
        iType = 'A',
        opcode = 14
    },

    lte = {
        iType = 'A',
        opcode = 15
    },

    gt = {
        iType = 'A',
        opcode = 16
    },

    gte = {
        iType = 'A',
        opcode = 17
    },

    eq = {
        iType = 'A',
        opcode = 18
    },

    sub = {
        iType = 'A',
        opcode = 19
    },

    mov = {
        iType = 'A',
        opcode = 20,
        generate = function (dest, src1)
            return dest, src1, 'r0'
        end
    },

    mul = {
        iType = 'A',
        opcode = 21
    },

    div = {
        iType = 'A',
        opcode = 22
    },

    mod = {
        iType = 'A',
        opcode = 23
    },

    storeptr = {
        iType = 'A',
        opcode = 24,
        generate = function (dest, src1)
            return dest, src1, 'r0'
        end
    },

    loadptr = {
        iType = 'A',
        opcode = 25,
        generate = function (dest, src1)
            return dest, src1, 'r0'
        end
    },

    setptr = {
        iType = 'A',
        opcode = 26,
        generate = function (dest, src1)
            return dest, src1, 'r0'
        end
    },

    loadfn = {
        iType = 'BU',
        opcode = 27,
        generate = function (dest, src1, _, _, _, _, functions)
            return dest, functions[src1]
        end
    },

    ret = {
        iType = 'A',
        opcode = 28,
        generate = function (dest)
            return dest, 'r0', 'r0'
        end
    },

    setproto = {
        iType = 'A',
        opcode = 29,
        generate = function (dest, src1)
            return dest, src1, 'r0'
        end
    },

    getproto = {
        iType = 'A',
        opcode = 30,
        generate = function (dest, src1)
            return dest, src1, 'r0'
        end
    },

    concat = {
        iType = 'A',
        opcode = 31
    },
}
