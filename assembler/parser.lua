local Parser = {}

function Parser.new(generation)
    return {
        generation = generation
    }
end

local function stripComments(line)
    -- If it contains trailing whitespace, remove it
    line = line:match("%s*(.*)")
    -- If it contains a comment, strip it
    local newLine = line:match("%s*([^;]*);.*")
    if newLine then
        return newLine
    end

    return line
end

local function splitLine(line)
    local split = {}

    while line ~= '' do
        local _, finish, match = line:find("^([^%s]+)%s*")
        split[#split + 1] = match

        if finish then
            line = line:sub(finish + 1)
        else
            break
        end
    end

    return split
end

local function parseConstant(value)
    local int = value:match('(%d+)i')
    if int then
        return int, 'int32'
    end

    local double = value:match('(%d+%.%d*)d')
    if double then
        return double, 'number'
    end

    error('Unknown constant type ' .. value)
end

local function parseEscapeSequences(string)
    return string:gsub("\\(.)", function (char)
        if char == 'n' then return '\n'
        elseif char == 't' then return '\t'
        elseif char == '"' then return '"'
        elseif char == 'r' then return 'r'
        elseif char == '\\' then return ''
        else return char
        end
    end)
end

function Parser:line(line)
    -- Strip comments and leading whitespace
    line = stripComments(line)

    -- If its a label
    local label = line:match(':(%w+)')
    if label then
        self.generation:visitLabel(label)
        return
    end

    -- If it is a constant
    local constant = line:match('%.(%w+)')
    if constant then
        local split = splitLine(line)
        local value, t = parseConstant(split[2])
        self.generation:visitConstant(constant, value, t)
        return
    end

    -- If it is a string
    local string, value = line:match('%$(%w+)%s+"(.*)"')
    if string then
        value = parseEscapeSequences(value)
        self.generation:visitString(string, value)
        return
    end

    -- If its a function
    local name, numArgs, numUpvals = line:match('~(%w+)%s+(%d+)%s+(%d+)')
    if name then
        self.generation:visitFunction(name, tonumber(numArgs), tonumber(numUpvals))
        return
    end

    -- Split by whitespace
    local split = splitLine(line)
    if #split > 0 then
        self.generation:visit(table.unpack(split))
    end
end

local Parser_mt = {__index = Parser}
return function (generation)
    return setmetatable(Parser.new(generation), Parser_mt)
end
