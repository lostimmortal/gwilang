#!/usr/bin/env lua

local Parser = require('assembler.parser')
local test = require('assembler.test_helper')

local fakeGeneration = {}
function fakeGeneration:visit (...)
    self.visited = {...}
end
function fakeGeneration:visitLabel(label)
    self.visitedLabel = label
end
function fakeGeneration:visitConstant(constant, value, t)
    self.visitedConstant = {
        name = constant,
        value = value,
        t = t
    }
end
function fakeGeneration:visitString(name, value)
    self.visitedString = {
        name = name,
        value = value
    }
end
function fakeGeneration:visitFunction(name, numArgs, numUpvals)
    self.visitedFunction = {
        name = name,
        numArgs = numArgs,
        numUpvals = numUpvals
    }
end

local p
test.setup(function ()
    fakeGeneration.visited = nil
    fakeGeneration.visitedLabel = nil
    p = Parser(fakeGeneration)
end)

test('parser handles comments', function ()
    p:line('; comment')
    assert(fakeGeneration.visited == nil)
    p:line('add r1 r4 r6 ; some other comment')
    assert(fakeGeneration.visited[1] == 'add')
end)

test('can extract opcodes', function ()
    p:line('add r1 r4 r6')
    assert(fakeGeneration.visited[1] == 'add')
    assert(fakeGeneration.visited[2] == 'r1')
    assert(fakeGeneration.visited[3] == 'r4')
    assert(fakeGeneration.visited[4] == 'r6')
end)

test('can find labels', function ()
    p:line(':mylabel')
    assert(fakeGeneration.visitedLabel == 'mylabel')
end)

test('can generate integer constants', function ()
    p:line('.constant 35i')
    assert(fakeGeneration.visitedConstant.name == 'constant')
    assert(fakeGeneration.visitedConstant.t == 'int32')
    assert(fakeGeneration.visitedConstant.value == '35')
end)

test('can generate floating point constants', function ()
    p:line('.constant 35.4d')
    assert(fakeGeneration.visitedConstant.name == 'constant')
    assert(fakeGeneration.visitedConstant.t == 'number')
    assert(fakeGeneration.visitedConstant.value == '35.4')
end)

test('can generate string constants', function ()
    p:line('$string "some string"')
    assert(fakeGeneration.visitedString.name == 'string')
    assert(fakeGeneration.visitedString.value == 'some string')
end)

test('can generate string constants with escapes', function ()
    p:line('$string "some string\\n"')
    assert(fakeGeneration.visitedString.name == 'string')
    assert(fakeGeneration.visitedString.value == 'some string\n')
end)

test('can generate empty string constants', function ()
    p:line('$emptyStr ""')
    assert(fakeGeneration.visitedString.name == 'emptyStr')
    assert(fakeGeneration.visitedString.value == '')
end)

test('can generate functions', function ()
    p:line('~testfn 1 4')
    assert(fakeGeneration.visitedFunction.name == 'testfn')
    assert(fakeGeneration.visitedFunction.numArgs == 1)
    assert(fakeGeneration.visitedFunction.numUpvals == 4)
end)

test.run()
