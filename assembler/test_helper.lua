local testHelper = {
    setupFns = {},
    teardownFns = {},
    tests = {}
}

function testHelper.setup(fn)
    testHelper.setupFns[#testHelper.setupFns + 1] = fn
end

function testHelper.teardown(fn)
    testHelper.teardownFns[#testHelper.teardownFns + 1] = fn
end

function testHelper.test(name, fn)
    testHelper.tests[name] = fn
end

function testHelper.run()
    local numTests = 0

    for name, test in pairs(testHelper.tests) do
        numTests = numTests + 1
        for _, s in ipairs(testHelper.setupFns) do
            s()
        end

        local ok, err = pcall(test)

        if not ok then
            print('Test "' .. name .. '" found an error: ' .. err)
            os.exit(1)
        end

        for _, t in ipairs(testHelper.teardownFns) do
            t()
        end
    end

    print(numTests)
end

return setmetatable(testHelper, {__call = function (_, ...)
    testHelper.test(...)
end})
