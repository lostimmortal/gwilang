# Bytecode format {#bytecode}

The bytecode is split into multiple sections, starting with a header and
ending with the opcodes. Each section will be described in turn. All numbers
are little endian.

## Header

This is simply the 32-bit number 0x4757494C followed by a version (currently
1) which will be increased once I start caring about backwards compatibility.

## Constant Section

This starts with a 64-bit unsigned integer at most 2^16 as the number of
constants. This is then followed by Objects of type OBJECT_INT and
OBJECT_NUMBER. Only these types are allowed. This can be referred to
using the loadk instruction, and is used to store floating point numbers
and numbers which are larger than what can be stored in a 16-bit signed
integer.

## String section

This is a little more complicated. It starts with the number of strings as a
64-bit unsigned integer at most 2^16. This is then followed by an array
of increasing 32-bit unsigned integers which reference the ending position
of the strings stored in the next section. See the bytecode verification
tests for examples of how this works. This is then padded to ensure it is
a multiple of 64-bits in length. Then, the string data all concatenated follows
this which is also padded to a multiple of 64-bit length.

The strings are stored with this strange array layout so that we have O(1)
time access to any string in the data section without having to do any
calculation in advance.

## Function section

The function section contains references to the bytecode. This section
starts off with the number of functions as a 64-bit unsigned integer at
most 2^16 followed by that number of 64-bit blocks with the following
format:

```
struct functionInformation {
    uint8_t numberOfArguments;
    uint8_t numberOfUpvalues;
    uint16_t padding;
    uint32_t bytecodeLocation;
};
```

The loadfn instruction (as shown below in the Opcodes section) will
load a given function index to the given register (%1). It will take
the values of %(1+1), %(1+2), ..., %(1+numberOfUpvalues) as its upvalues.

The VM will start execution at the last function on the list, and will
terminate execution when either the this function returns or execution
runs off the end.

## Opcode format

The m6 vm is register based. Each instruction is exactly 32 bits in
size. All instructions one of 2 basic formats:

* An 8 bit opcode, followed by three 8 bit register targets (the first being
the destination and the last two being sources). This will be referred to as
type A in the rest of this document, the registers being referred to as %1,
%2 and %3 respectively
* An 8 bit opcode, followed by one 8 bit register target and a 16
bit short (either signed or unsigned depending on the instruction).
This will be referred to as type B in the rest of this document with
the register being referred to as %1 and the short referred to as 'data'.
If the data is signed, then it is refered to as B(S), if unsigned as B(U).

Each instruction is stored individually as a 32-bit unsigned integer with
the data section being stored in little endian

## Registers

There are 256 available registers available for addressing. Each register
holds a single value of one of the basic types. When a function is being
executed, registers 0 to the maximum number of arguments - 1 are filled with
relevant arguments passed to the function. When returning, any register
can be chosen to start the return value.

## Jumps

All jumps are relative and stored in the data section of the B type opcode.
Conditional jumps will use the register in some way and unconditional jumps
will ignore the provided register. The size of the jump is the number of opcodes
to move the instruction pointer, so a jump of size 1 is the same as a nop
and a jump of size 0 is a jump to itself.

## Opcodes

The following table lists the opcodes available, along with their assembler
name.

| Name      | Opcode | Type | Discussion |
|-----------|--------|------|------------|
| nop       | 0      | N/A  | Does nothing |
| loadshort | 1      | B(S) | %1 = data |
| add       | 2      | A    | %1 = %2 + %3 |
| jmp       | 3      | B(S) | data is relative jump, %1 is ignored |
| addk      | 4      | A    | %1 = %2 + (third register as unsigned 8 bit number) |
| loadbool  | 5      | B(S) | %1 = data != 0 |
| jt        | 6      | B(S) | Jump if %1 is truthy |
| loadk     | 7      | B(U) | Load constant with index equal to the data |
| loadstr   | 8      | B(U) | Load string into %1 |
| dictnew   | 9      | B(U) | Create a new Dict on %1 with initial size data |
| dictput   | 10     | A    | %1[%2] = %3 |
| dictget   | 11     | A    | %1 = %2[%3] |
| call      | 12     | A    | %1 = %2(%(2 + 1), %(2 + 2), ..., %(2 + (third register as an unsigned 8 bit number))) |
| loadg     | 13     | A    | %1 = globals[%2] |
| lt        | 14     | A    | %1 = %2 < %3 |
| lte       | 15     | A    | %1 = %2 <= %3 |
| gt        | 16     | A    | %1 = %2 > %3 |
| gte       | 17     | A    | %1 = %2 >= %3 |
| eq        | 18     | A    | %1 = %2 == %3 |
| sub       | 19     | A    | %1 = %2 - %3 |
| mov       | 20     | A    | %2 = %1 (move %1 to %2) |
| mul       | 21     | A    | %1 = %2 * %3 |
| div       | 22     | A    | %1 = %2 / %3 |
| mod       | 23     | A    | %1 = %2 % %3 |
| storeptr  | 24     | A    | %1 = &%2 (store a pointer and put address in %1) |
| loadptr   | 25     | A   | %1 = \*%2 (dereference a pointer and put the value in %1) |
| setptr    | 26     | A    | \*(%1) = %2 |
| loadfn    | 27     | B(U) | %1 = function referred to by %2 (see notes) |
| ret       | 28     | A    | Returns %1 from the current function |
| setproto  | 29     | A    | Set the prototype of %1 to %2 |
| getproto  | 30     | A    | Set %1 to the prototype of %2 |
| concat    | 31     | A    | %1 = %2 ++ %3 (string concatenation) |
