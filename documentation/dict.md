# Dict implementation details {#dict}

The Dict data structure allows for key value storage of Objects. They are stored
in a hash table with fast insertion, deletion and searching. This document explains
the storage mechanisms.

## Robin Hood Hash Tables

The problem with regular hash tables where failing to find a slot for a specific
key will result in a linear search for the first free slot is that often these
slots can end up being quite far away from the original chosen location in fuller
hash tables. In Java, the hash table will resize when it is more than 2 thirds full
which is quite wasteful in terms of space. Robin Hood hash tables can be much
fuller before needing to resize and also keeps search times lower.

The key concept is to keep the distance from the initial bucket chosen for a given
key to a minimum. We shall call this distance the "distance to the initial bucket"
or DIB for short. With each key - value pair, we store its DIB. When inserting
a value into the hashtable, we check to see if there is an element in the initial
bucket. If there is, we move onto the next value. Then, we check to see if there
is already a value there and if there is, we check its DIB. If our DIB is greater
than or equal to the DIB of the value that is already there, we swap the working
value and the current entry. This continues until an empty slot is found.

Using this technique, the average DIB is kept fairly low which decreases search
times. As a simple search time optimisation, the maximum DIB is kept track of
and so we will only need to search as far as that in order to know whether or
not a given element is in the hash table.

### Deletion

Special mention is given to deletion. Unfortunately, the maximum DIB is not
kept track of after deletion, since we would not only need to keep track of
the maximum DIB but also the number of elements which have that maximum DIB.

In order to delete an element, we search to see if it exists. If it does,
we remove that element and then iterate through the array from that point
and move each element back one space (updating its DIB). If the DIB of the
element we are trying to remove is 0, then we stop this algorithm.

This means that deletion will lower the average DIB of the elements and
keep searching and insertion fast.

## Rehashing

Currently, no incremental rehashing takes place. Once the Dict becomes
more than 85% full (this value may change when I do some benchmarking),
a new list is allocated with double the capacity and the entire node list
is migrated. In future, this can be done incrementally to prevent one
addition causing a pause as nodes are reallocated.

## Iteration

Iteration is done by storing the state of the iterator in a single int.
The algorithm iterates through each element in the node array and will
return once it finds one. Multiple iterators are supported but modifying
the Dict during iteration results in undefined behaviour.
