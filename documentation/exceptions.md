# Exception handling implementation details {#exceptions}

Here I will try to explain how the TRY, CATCH, FINALLY, RETURN and
THROW macros work, along with the general idea of the exception handling
available in the C code within m6.

## Motivation

Due to C's lack of standard error handling, there are 2 main ways of
managing errors which are commonly seen.

1. Returning special 'error values'.
2. Setting a global error value

I will explain why I don't like either of these solutions. Return special
error values was how m6 started out. However, there are many situations
where you do not check these values (due to laziness, or mistakes). This
can lead to many bad situations arising. This can be partially mitigated
with compiler attributes saying 'ignoring the return value for this method
is an error' but this then leads to the second issue with this method
of error handling. It often ends up being quite verbose. Constantly
having to do an 'if' statement after every call gets tedious and full
of boiler plate. It also has the problem that we now have to handle the
return value of the method in an argument rather than actually using
the return value. This ends up with even more boiler plate.

Setting a global error value solves the 'cannot use the return value'
problem but introduces a new one. The convenient attributes on methods
stating that the return value must be handled is now gone so we cannot
rely on the compiler to manage our error handling any more. Also, this
doesn't solve the issue of boiler plate.

## Prior art

This would have been much harder without some prior art which
I started with. The initial implementation came from here:
http://www.di.unipi.it/~nids/docs/longjump_try_trow_catch.html but this
didn't handle all the cases I wanted. I wanted the ability to catch all
exceptions as well, so I implemented a solution which allowed either
CATCH_ALL or FINALLY or neither (but not both). However, I was never
happy with this solution so in the end I now have a mostly original
implementation using hacks as never before seen (or at least I've never
seen them before).

## High level explanation

The basic idea here is that we maintain an exception call stack. This
gets pushed to every time we hit a TRY statement (using setjmp) and then
when THROW is called, it takes the highest element in the call stack and
longjmps to it. At the end of the TRY block, the call stack is popped. If
there is an error, we call the CATCH routine and provide a method for the
user to rethrow the exception if it cannot be handled in this level. For
more information of this overview, see the reference in the prior art.

## Crazy C macro magic

In order to prevent the need for a closing tag (such as the ETRY in the
reference), we use a simple trick. Since the current implementation
is done using a set of if statements, this was relatively easy to
implement. For future reference, if you want to write a macro that runs
something after the first invocation, the following can be used:

```
#define MY_MACRO for (int runTwice__ = 0; runTwice__ < 2; runTwice__++) \
    if (runTwice == 1) { \
      /* Do whatever you want after the user's code here */ \
    } else
```

This works due to C allowing braces to be missed if there is only one
expression. To make matters more exciting, this macro even follows those
rules, so

```
MY_MACRO doAThing();
```

Does what you would expect. The TRY macro has both initialisation and
after work to do and both of these will be explained in later sections.

The RETURN macro makes use of the do...while trick that is well explained
elsewhere. However, 3 GCC extensions are used in this code. The first
being `__label__` which allows for a local label. Hence you can have more
than one RETURN statement in a single function. Also used is `__auto_type`
which is used to allow for immediate calculation of the return value and
defer the actual returning for later. Finally there is also the computed
goto which is used twice within the macro.

Ensure you are familiar with all there extensions before continuing,
documentation on these can be found in the excellent GCC manual:
https://gcc.gnu.org/onlinedocs/gcc-4.9.2/gcc/C-Extensions.html#C-Extensions

## Detailed explanation

This section will not follow a coherent story top to bottom. It may
require a few readings to work out what is going on here but it is worth
it in the end.

### TRY

We need to use some awful hacks here. Firstly, because you can only
initialise one variable in a for loop, we need to create a temporary
struct to store the values in. The contents of this struct will be
referred to as local variables for the rest of this section (since that
is what we are using them for). An explanation of these variables is
as follows:

* tryAttempt The return value from setjmp. Will be 0 on the first call, > 0 on subsequent ones
* runFourTimes This get incremented by 1 each loop, and is used to ensure the relevant bits of the for loop get executed only once.
* returnTo A label to jump to after the finally block is finished executing
* continueLabel To break out of the current TRY or CATCH block, goto this label

Then, some initialisation code happens. This is done by checking if
runFourTimes is 0. If it is, then set the continue label to the end of the
if block. This is used rather then just a `continue` statement because a
common pattern is RETURNing inside a for loop (see the section on return).

Next, if we are on our final iteration (so after FINALLY has run),
we check to see if any exception thrown has been handled. If not, the
exception is rethrown to the next TRY block. If it has, then we pop the
exception handling stack. Finally, if we have somewhere to go after the
FINALLY block has been executed, we goto that. This is the end of the
TRY .. CATCH block.

Lastly in the TRY macro, we check to see that we are on the second
iteration and if tryAttepmt is 0, then setjmp returned zero and we
should run the contents of the TRY block. This works as explained in
the C macro magic section above.

### CATCH

This is very simple, we check that we are on our second iteration and
if longjmp returned the value of the exception we are interested in,
then we execute the contents of this block. The only strange bit is the
`&& (catchHandled__() || 1)` which just ensures that `catchHandled__()`
is called but that it returning false doesn't stop the execution of the
CATCH statement. `catchHandled__()` will set the handled flag to true
(which is used by the end of the TRY statement to check that something
handled the exception) and will return the old value.

### CATCH_ALL

This is similar. The first check is identical, except we don't care for
the value of the tryAttempt (other then it can't be 0). The for loop is
used to ensure that we have a local available to us and is written to
ensure that the contents of the loop only executes once.

### FINALLY

Probably the simplest one. The finally block executes in the third
iteration of the loop.

### RETURN

We want to be able to return from within the TRY and CATCH blocks. This
would be great but we also still want the FINALLY block to execute. The
solution here is to define a macro that will save the value we would
like to return, continue on to the finally block and then return. This
has been done by storing the return value, and setting returnTo to the
return statement. We cannot use continue here (since that would just do
it on the do - while loop but even then it would fail for nested loops)
so instead we goto the continue label set in the TRY statement.
