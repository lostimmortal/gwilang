# GC Details {#GC}

The garbage collector is a Semi-Space design. It stores all objects in
a large array which is 64-bit aligned. Each object has a 64-bit header
(a GCEntry) with a 32 bit header and then a 32-bit size field. This size
field is used as a forward pointer if the type is POINTER.

Pointers when stored in Objects are 32 bit signed integers (negative array
indices, although not currently used could end up being used in future as
a method of not having to invalidate old indicies). This is the index to
the data array stored in the GC object which is used to get the element.

Only half the allocated space is available at any one time. When we run
out of space, all currently reachable old values are copied into the
unused space and the unreachable objects are left behind. This means that
we don't spend any time on unreachable objects when doing a collection.

A version of Cheney's algorithm is used to do the collection. This
allows for a non-recursive, highly cache friendly solution to doing
the collection.  It has the downside that the Dict object itself can
be quite far away (memory wise) from the keys and values it is storing,
but this is worth it for the ease at which the collection algorithm can
be implemented.

## GC Roots

The only thing the GC will keep after running a collection is what
is referenced in the GC roots. This is a Stack which will allow for
storage of arbitrary Objects. The VM can allocate the first element in
the Stack to be a Dict (for example) to store the global variables or
similar. This simplifies the collection algorithm since it only has to
deal with a single source of roots.

## Forwarding pointers

These are stored in-line in the header, and don't damage any of the data
stored in the collector. This fact is used to maintain the guarantee that
any data that an Object pointer points to will stay untouched (although
could be stale) by a collection after 1 collection cycle. This is used by
the resizing ability of the Dict (so far) so must be kept when resizing
of the GC data array is implemented (hence allowing for the possibility
of negative pointers).
