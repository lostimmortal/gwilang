package.path = BASE_DIRECTORY .. '/?.lua;' .. package.path

local depsFile = DEPS_FILE
DEPS_FILE = nil
BASE_DIRECTORY = nil

local depFile = io.open(depsFile, 'w')

require = function (file)
    local foundFile = package.searchpath(file, package.path)
    depFile:write(arg[0] .. ': ' .. foundFile .. '\n')
    return loadfile(foundFile)()
end
