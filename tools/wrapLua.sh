#!/usr/bin/env bash

# Wraps a lua call which will make a dependencies file
# pass arguments as follows:

# 1: The path to the lua executable
# 2: The base directory to require from
# 3: The file to put the dependencies into
# 4...: Any arguments to pass to lua

LUA=$1
BASE_DIRECTORY=$2
DEPS_FILE=$3
shift
shift
shift

"$LUA" \
    "-eBASE_DIRECTORY=\"$BASE_DIRECTORY\"" \
    "-eDEPS_FILE=\"$DEPS_FILE\"" \
    "-edofile(BASE_DIRECTORY .. \"/tools/luaRequireFix.lua\")" \
    $@
