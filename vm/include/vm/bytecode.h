/**
 * @file bytecode.h
 * @author Gwilym Kuiper
 * @brief The bytecode for Gwilang M6
 */
#pragma once

#include "object.h"
#include <stdint.h>

/// @addtogroup VM
/// @{

/**
 * @brief Opcodes available
 */
typedef enum {
    /** Type N/A, do nothing */
    NOP,
    /** Type B, load the short stored in data */
    LOADSHORT,
    /** Type A, %1 = %2 + %3 */
    ADD,
    /** Type B, jump relative to data */
    JMP,
    /** Type A, %1 = %2 + (third value as a constant) */
    ADDK,
    /** Type B, load true if non zero, false if zero */
    LOADBOOL,
    /** Type B, jumps if true */
    JT,
    /** Type B, loads constant referenced by data */
    LOADK,
    /** Type B, loads string referenced by data */
    LOADSTR,
    /** Type B, creates a Dictionary with initial size data */
    NEWDICT,
    /** Type A, %1[%2] = %3 */
    DICTPUT,
    /** Type A, %1 = %2[%3] */
    DICTGET,
    /** Type A, %1 = %2(%(2+1), %(2+2), ..., %(2 + (third register))) */
    CALL,
    /** Type A, %1 = globals[%2] */
    LOADG,
    /** Type A, %1 = %2 < %3 */
    LT,
    /** Type A, %1 = %2 <= %3 */
    LTE,
    /** Type A, %1 = %2 > %3 */
    GT,
    /** Type A, %1 = %2 >= %3 */
    GTE,
    /** Type A, %1 = %2 == %3 */
    EQ,
    /** Type A, %1 = %2 - %3 */
    SUB,
    /** Type A, %2 = %1 */
    MOV,
    /** Type A, %1 = %2 * %3 */
    MUL,
    /** Type A, %1 = %2 / %3 */
    DIV,
    /** Type A, %1 = %2 % %3 */
    MOD,
    /** Type A, %1 = &(%2) */
    STOREPTR,
    /** Type A, %1 = *(%2) */
    LOADPTR,
    /** Type A, *(%1) = %2 */
    SETPTR,
    /** Type B(U), %1 = function refered to by %2 and copies upvalues */
    LOADFN,
    /** Type A, return %1 */
    RET,
    /** Type A, set prototype of %1 to %2 */
    SETPROTO,
    /** Type A, %1 = prototype of %2 */
    GETPROTO,
    /** Type A, %1 = %2 ++ %3 */
    CONCAT,
} OPCODE;

/**
 * @brief The bytecode format as a 32-bit union
 *
 * See @ref bytecode for more information
 */
typedef union
{
    /** An A instruction */
    struct
    {
        /** Opcode */
        uint8_t opcode;
        /** Destination register */
        uint8_t dest;
        /** Source register 1 */
        uint8_t src1;
        /** Source register 2 */
        uint8_t src2;
    } A;

    /** A B instruction */
    struct
    {
        /** Opcode */
        uint8_t opcode;
        /** Destination register */
        uint8_t dest;
        /**
         * The data stored with the instruction.
         *
         * Depending on the instruction type is either signed
         * or unsigned
         */
        union
        {
            int16_t s;  /**< Signed data */
            uint16_t u; /**< Unsigned data */
        } data;
    } B;

    /** Compressed as a 32 bit integer */
    uint32_t data;
} Instruction;

/**
 * Contains information about the functions
 */
typedef struct
{
    uint8_t numUpvals;  /**< The number of upvalues this function has */
    uint8_t numArgs;    /**< The number of arguments this function takes */
    uint16_t padding__; /**< Padding bytes */
    uint32_t bytecodeLocation; /**< The location of the bytecode */
} FunctionData;

_Static_assert(sizeof(FunctionData) == 8,
               "Expect function data struct to be 64-bit");

/**
 * All information regarding the bytecode is stored here.
 * That includes number of constants, strings, the strings themselves
 * and the opcodes. All access to inner objects should be done by the
 * accessor methods, to ensure safety.
 *
 * Verification is done by default, so all data in this struct is
 * known to be safe to run without any bounds checking etc.
 */
typedef struct Bytecode Bytecode;

/**
 * @brief Error codes to do with bytecode verification
 *
 * Before the VM can execute bytecode, it must be verified. This is
 * central to the optimisations that the VM can do, since during
 * execution, it can assume that all its instructions are valid and
 * won't lead to a jump to an invalid memory location, derefencing
 * bad locations or overrunning strings.
 */
typedef enum {
    BYTECODE_NO_ERROR,          /**< There wasn't an error with verification */
    BYTECODE_BAD_HEADER,        /**< Bad header */
    BYTECODE_BAD_NUM_OBJECTS,   /**< Number of objects mal formatted */
    BYTECODE_BAD_CONSTANT,      /**< An illegal constant was found */
    BYTECODE_BAD_NUM_STRINGS,   /**< Number of strings mal formatted */
    BYTECODE_BAD_STRING_ENDS,   /**< Error in string ends list */
    BYTECODE_BAD_LENGTH,        /**< Non whole number of opcodes */
    BYTECODE_BAD_NUM_FUNCTIONS, /**< Number of functions mal formatted */
} BytecodeVerificationError;

/**
 * @brief Verifies and creates a Bytecode object from given bytecode
 * @param bytecode The bytecode data
 * @param len The length of the bytecode
 * @param error Will be set to the error code if verification fails
 * @return A new Bytecode object or NULL if there was an error
 *
 * The Bytecode object returned will have references to the bytecode
 * param passed. Hence, the bytecode param must live at least as long
 * as the Bytecode object is used. This is in order to save memory
 * usage.
 */
Bytecode *Bytecode_new(const char *bytecode, int len,
                       BytecodeVerificationError *error);

/**
 * @brief Frees a Bytecode object.
 * @param bytecode The bytecode to free
 *
 * This does not free the original source of the bytecode, that must be
 * done separately.
 */
void Bytecode_free(Bytecode *bytecode);

/**
 * @brief Gets the number of instructions in the bytecode
 * @param bytecode The Bytecode object
 * @return The number of instructions
 */
int Bytecode_numInstructions(Bytecode *bytecode);
/**
 * @brief The Instructions themselves
 * @param bytecode The Bytecode object
 * @return The instructions stored within the bytecode
 */
Instruction *Bytecode_instructions(Bytecode *bytecode);
/**
 * @brief Gets the nth constant
 * @param bytecode The Bytecode object
 * @param n The numbered constant to get
 * @return The constant
 */
Object Bytecode_constant(Bytecode *bytecode, int n);
/**
 * @brief Gets the nth string
 * @param bytecode The Bytecode object
 * @param gc The GC to store the string into
 * @param n The string to get
 * @return The string Object
 */
Object Bytecode_string(Bytecode *bytecode, GC *gc, int n);
/**
 * @brief Gets the number of functions
 * @param bytecode The Bytecode object
 * @return The number of functions
 */
int Bytecode_numFunctions(Bytecode *bytecode);
/**
 * @brief Gets the data on the nth function
 * @param bytecode The bytecode object
 * @param n The function to get the data of
 * @return The function data
 */
FunctionData *Bytecode_function(Bytecode *bytecode, int n);

/// @}
