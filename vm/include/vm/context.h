/**
 * @file context.h
 * @brief Contains the context in which a cfunction is called
 * @author Gwilym Kuiper
 */
#pragma once

#include "stack.h"

/// @addtogroup VM
/// @{

typedef struct VM VM;

/**
 * @brief Contains the context a cfunction is called
 */
typedef struct Context
{
    /** The VM object the function was called from */
    VM *vm;
    /** The StackView the Context has */
    StackView view;
} Context;

/// @}
