/**
 * @file object.h
 * @brief All functionality for managing Objects in m6.
 * @author Gwilym Kuiper
 */
#pragma once

#include <stdbool.h>
#include <stdint.h>

// Forward reference
typedef struct GC GC;
typedef struct Context Context;

/// @addtogroup VM
/// @{

/**
 * @brief The possible object types
 */
typedef enum {
    /** Anything for which IS_OBJECT() returns false will be an OBJECT_NUMBER */
    OBJECT_NUMBER = 1,
    /** NULL singleton */
    OBJECT_NULL,
    /** Booleans, true will store 1 in the data, false will store 0 */
    OBJECT_BOOLEAN,
    /** int32 is used for storing smaller numbers to avoid floating point stuff
       */
    OBJECT_INT,
    /** Strings are stored as a pointer in the lower bits */
    OBJECT_STRING,
    /** Dictionaries used for everything else */
    OBJECT_DICTIONARY,
    /** M6 Function */
    OBJECT_FUNCTION,
    /** C function */
    OBJECT_CFUNCTION,
    /** A pointer to an Object */
    OBJECT_POINTER,
    /** The last type */
    OBJECT_TYPE_LAST,
} OBJECT_TYPE;

/**
 * @brief The basic object used by everything in m6.
 *
 * Objects are stored by packing everything into 64 bits.
 * Considering the bits as an IEEE 754 double precision
 * binary floating point number, we take the object to be
 * a number if the exponent is not all 1s (i.e. is not a
 * NaN), and if it is a NaN, we use the lower 52bit significand
 * to store additional data about the object.
 *
 * This makes copying objects very cheap, and allows for easy
 * storage too. See the individual creation functions for more
 * information about the storage of non-numbers.
 *
 * If number is NaN, then the significand must be non-zero,
 * so we start object types at 1 to ensure this. Hence,
 * by applying the IS_OBJECT_MASK, if this is non-zero, then
 * packed into the Object datatype is something which is not
 * actually a double.
 *
 * <pre>
 * +------+--------+-----------------+
 * |sign  |exponent|significand      |
 * |1bit  |11bits  |52bits           |
 * +------+--------+-----------------+
 * |unused|111...11|[objtype:4][data]|
 * +------+--------+-----------------+
 * </pre>
 *
 * objtype is the type of object, using 4 bits
 * defined in the enum OBJECT_TYPE, and data depends on
 * the object type.
 */
typedef union
{
    /** The raw data */
    uint64_t data;
    /** If it's a number, stored here */
    double number;
    /** In order to get a signed lower 32-bits (yay for endianness...)*/
    struct
    {
        int32_t bottom;
        uint32_t top;
    } split;
    /** All the data in an easier to reach struct */
    struct __attribute__((packed))
    {
        int32_t data : 32;
        uint16_t extra : 16;
        OBJECT_TYPE type : 4;
        int32_t reserved : 12;
    } fullySplit;
} Object __attribute__((transparent_union));

_Static_assert(sizeof(Object) == 8, "Expected Object to be 64-bits in size");

/**
 * @brief A C function used for C interop
 * @param ctx The context the function was called in
 * @return The return value of the function
 */
typedef Object (*cfunction)(Context *ctx, int n);

/**
 * @brief Get the type of a given object
 * @param o The object to test
 * @return The object type
 */
uint8_t Object_type(Object o);
/**
 * @brief Returns a new blank object of a given type
 * @param type The type to give the object
 * @return An Object of type type
 */
Object Object_newOfType(uint8_t type);

/**
 * @brief Creates a null object
 * @return A null object
 */
Object Object_newNull(void);
/**
 * @brief Will create a new object from the number n
 * @param n The value to give the new object
 * @return A new Object with value n
 */
Object Object_newNumber(double n);
/**
 * @brief Returns the number stored in the object o
 * @param o The Object storing the number
 * @return The number stored in the object
 */
double Object_number(Object o);
/**
 * @brief Returns the object casted to a number
 * @param o The object storing the number
 * @return The number stored in the object
 *
 * This method differs from Object_number because if the stored type
 * is an OBJECT_INT, then it will be cast to a double
 */
double Object_toNumber(Object o);
/**
 * @brief Creates a new boolean
 * @param b Zero for false, non zero for true
 * @return A new Object of type boolean
 *
 * Booleans are stored with the lowest bit 0 if false and 1 if true
 */
Object Object_newBoolean(bool b);
/**
 * @brief Returns the value of a boolean (does not check type)
 * @param o The Object to return the boolean from
 * @return A boolean
 */
#define Object_boolean(o) ((bool) (o.data & 1))
/**
 * @brief Creates an Object storing a 32 bit signed integer
 * @param i The integer value of the new object
 * @return The integer
 */
Object Object_newInt(int32_t i);
/**
 * @brief Returns the value of an int (does not check type)
 * @param o The Object storing the int
 * @return An int32_t
 */
#define Object_int(o) (o.split.bottom)
/**
 * @brief Gets the pointer value out of an object
 * @param o The Object storing a pointer
 * @return A pointer
 */
#define Object_pointer(o) (o.split.bottom)
/**
 * @brief Creates an Object storing a string with a given length
 *
 * Note that this string object will not necessarily be valid
 * after a call to GC_collect, GC_alloc* or GC_free.
 *
 * @param gc The GC object to store the string in
 * @param str The string object to store
 * @param len The length of the string
 * @return A new object related to the string
 */
Object Object_newString(GC *gc, int32_t len, const char *str);
/**
 * @brief Gets the length of a given string
 * @param gc The GC the string is stored in
 * @param str The object returned from newString
 * @return The length of the string (or -1 if not a string)
 */
int32_t Object_stringLen(GC *gc, Object str);
/**
 * @brief Gets the string data
 * Note that the str argument is a pointer. You should ensure that the return
 * value of this function does NOT exceed the lifetime of the str argument.
 * @param gc The GC the string is stored in
 * @param str The object returned from newString
 * @return The string data
 */
const char *Object_string(GC *gc, Object *str);
/**
 * @brief Concatenates two strings
 * @param gc The GC object the strings are in
 * @param str1 The first string
 * @param str2 The second string
 * @return The concatenation of the strings
 */
Object Object_stringConcat(GC *gc, Object str1, Object str2);
/**
 * @brief Adds two Objects (as numbers)
 * @param a The first object
 * @param b The second
 * @return The sum as an Object
 */
Object Object_add(Object a, Object b);
/**
 * @brief Subtracts two Objects (as numbers)
 * @param a The first object
 * @param b The second
 * @return The difference as an Object
 */
Object Object_sub(Object a, Object b);
/**
 * @brief Multiplies two Objects (as numbers)
 * @param a The first object
 * @param b The second
 * @return The product as an Object
 */
Object Object_mul(Object a, Object b);
/**
 * @brief Divides two Objects (as numbers)
 * @param a The first object
 * @param b The second
 * @return The result of the division as an Object
 */
Object Object_div(Object a, Object b);
/**
 * @brief Takes the modulo of two objects (as numbers)
 * @param a The first object
 * @param b The second
 * @return The remainder after dividing a by b
 *
 * If a and b are integers, then just returns a % b.
 * If either a or b are numbers, returns a - floor(a / b) * b
 */
Object Object_mod(Object a, Object b);
/**
 * @brief Creates a Dict object
 * @param gc The GC to create the Dict in
 * @param initialCapacity The capacity to give the new dict
 * @return The dict Object
 */
Object Object_newDict(GC *gc, int initialCapacity);
/**
 * @brief Create a new Dict object from a pointer
 * @param pointer The pointer the new Dict object should have
 * @return The object
 */
Object Object_newDictFromPointer(int32_t pointer);
/**
 * @brief Puts an object into the dict
 *
 * dict, key and value must be in the same GC object.
 *
 * @param gc The GC object the dict is in
 * @param dict The Dict to put the key-value pair into
 * @param key The key
 * @param value The value
 * @return The dict object
 */
Object Object_dictPut(GC *gc, Object dict, Object key, Object value);
/**
 * @brief Gets an Object specified by key from the dict
 * @param gc The GC object the dict, key and value are in
 * @param dict The dict Object
 * @param key The key
 * @return The value the key is stored to or OBJECT_NULL otherwise
 */
Object Object_dictGet(GC *gc, Object dict, Object key);
/**
 * @brief Gets the length of the dictionary
 * @param gc The GC object the dict is in
 * @param dict the dict Object
 * @return The length of the dictionary (of type OBJECT_NUMBER)
 */
Object Object_dictLen(GC *gc, Object dict);
/**
 * @brief Sets the prototype of a dict
 * @param gc The GC object the dict and the proto are in
 * @param dict The dictionary to set the prototype of
 * @param proto The prototype to set (or null to unset)
 */
void Object_dictSetProto(GC *gc, Object dict, Object proto);
/**
 * @brief Gets the prototype of a dict
 * @param gc The GC object the dict and the proto are in
 * @param dict The dictionary to get the prototype of
 * @return The dictionaries proto or null if not set
 */
Object Object_dictGetProto(GC *gc, Object dict);
/**
 * @brief Creates a new CFunction object
 * @param gc The GC object to create the function in
 * @param fn The function to store
 * @return The Object
 */
Object Object_newCFunction(GC *gc, cfunction fn);
/**
 * @brief Creates a new pointer to location
 * @param location The location the pointer should point to
 * @return A new Object of type OBJECT_POINTER
 */
Object Object_newPointer(int32_t location);
/**
 * @brief Calls a cfunction object
 * @param gc The GC object the cfunction is stored
 * @param o The cfunction Object
 * @param ctx The context to pass the function
 * @param n The number of arguments the function was called with
 * @return The return value of the cfunction
 */
Object Object_call(GC *gc, Object o, Context *ctx, int n);
/**
 * @brief Whether an Object is truthy
 * @param o The object to test
 * @return true if the Object is truthy
 *
 * An object is truthy if it is not false or null
 */
bool Object_isTruthy(Object o);
/**
 * @brief Returns the hash code of the Object
 * @param gc The GC object (used for string hashing)
 * @param o The object to hash
 * @return The hash code of the object
 */
uint64_t Object_hash(GC *gc, Object o);
/**
 * @brief Returns whether 2 objects are equal
 * @param gc The GC object the objects are in
 * @param a The first object
 * @param b The second object
 * @return Whether the two objects are equal
 */
bool Object_isEqual(GC *gc, Object a, Object b);
/**
 * @brief Returns whether a is less than b
 * @param gc The GC object a and b are defined in
 * @param a The first object
 * @param b The second object
 * @return Whether the a < b
 */
bool Object_isLessThan(GC *gc, Object a, Object b);
/**
 * @brief Returns whether a is less than or equal to b
 * @param gc The GC object a and b are defined in
 * @param a The first object
 * @param b The second object
 * @return Whether the a <= b
 */
bool Object_isLessThanOrEqualTo(GC *gc, Object a, Object b);
/**
 * @brief Returns whether a is greater than b
 * @param gc The GC object a and b are defined in
 * @param a The first object
 * @param b The second object
 * @return Whether a > b
 */
bool Object_isGreaterThan(GC *gc, Object a, Object b);
/**
 * @brief Returns whether a is greater than or equal to b
 * @param gc The GC object a and b are defined in
 * @param a The first object
 * @param b The second object
 * @return Whether a >= b
 */
bool Object_isGreaterThanOrEqualTo(GC *gc, Object a, Object b);
/**
 * @brief Creates a new function object
 * @param gc The GC object to create the function in
 * @param numArgs The number of arguments the function takes
 * @param numUpvals The number of upvalues the function has
 * @param bytecodeLocation The bytecode location for the function
 * @return The new function
 */
Object Object_newFunction(GC *gc, uint8_t numArgs, uint8_t numUpvals,
                          uint32_t bytecodeLocation);
/**
 * @brief Gives the number of arguments a function takes
 * @param fn The function
 * @return The number of argmuents
 */
uint8_t Object_numArguments(Object fn);
/**
 * @brief Gives the number of upvalues a function has
 * @param fn The function
 * @return The number of upvalues
 */
uint8_t Object_numUpvalues(Object fn);
/**
 * @brief Gives the bytecode location of a function
 * @param gc The GC object the function was allocated into
 * @param fn The function
 * @return The bytecode location
 */
uint32_t Object_bytecodeLocation(GC *gc, Object fn);
/**
 * @brief Sets the nth upvalue for a function
 * @param gc The GC object the function was allocated into
 * @param fn The function to get the upvalue of
 * @param n The index of the upvalue to set
 * @param obj The object to set the upvalue to
 */
void Object_setUpvalue(GC *gc, Object fn, int n, Object obj);
/**
 * @brief Gets the nth upvalue for a function
 * @param gc The GC object the function was allocated into
 * @param n The index of the upvalue to get
 * @param fn The function to get the upvalue of
 * @return obj The upvalue
 */
Object Object_getUpvalue(GC *gc, Object fn, int n);

/// @}
