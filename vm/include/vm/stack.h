/**
 * @file stack.h
 * @brief A simple dynamic stack for storing objects.
 * @author Gwilym Kuiper
 */
#pragma once

#include "object.h"

/// @addtogroup VM
/// @{

/**
 * @brief A dynamic stack
 *
 * The data is stored as a simple array. But beware that the array
 * may be moved since it could need resizing. Hence, views are provided
 * which will handle this complication for you.
 *
 * This isn't a traditional stack in the sense that one element is
 * added at a time. Entire chunks are inserted and removed with more
 * than 1 element at a time. This is for the virtual machine to be
 * able to support as many registers as it can. An entire stack frame
 * is pushed in one go, and popped also.
 */
typedef struct
{
    /** The size of the stack */
    int size;
    /** The current top of the stack */
    int top;
    /** The objects stored */
    Object *objects;
    /** The current number of stack views */
    int depth;
    /** The current stack view sizes */
    int *stackViews;
} Stack;

/**
 * @brief Creates a new stack
 * @return A new stack
 */
Stack *Stack_new(void);
/**
 * @brief Frees the provided stack
 * @param s The stack to free
 */
void Stack_free(Stack *s);

/**
 * @brief A view into the stack.
 *
 * This cannot have its size adjusted, and is very attached to its
 * parent Stack object. Ensure that stack views are popped in the
 * reverse order that they are pushed, or bad things will happen.
 *
 * Once a StackView has been popped, it is no longer safe to use.
 */
typedef struct
{
    /** The size of the StackView */
    int size;
    /** The parent Stack */
    Stack *parent;
    /** The index into the Stack this StackView is based */
    int index;
} StackView;

/**
 * @brief Pushes a new chunk onto the stack
 * @param s The stack to push these elements on to
 * @param n The size of the view to push
 * @returns A new StackView which can be used to manipulate this area
 */
StackView Stack_push(Stack *s, int n);
/**
 * @brief Removes a StackView from the stack
 * @param s The stack to remove from
 * @return The new top StackView
 */
StackView Stack_pop(Stack *s);

/**
 * @brief Sets the n^{th} object in the StackView view to the object o
 * @param view The StackView to alter
 * @param n The index (0 = first)
 * @param o The object to insert
 * @return 0 if successful, 1 if out of bounds
 */
void StackView_put(StackView view, int n, Object o);
/**
 * @brief Gets the contents of the StackView at position n
 * @param view The stack view
 * @param n The position to get
 * @return The contents of the stack at that position
 *
 * Behaviour is undefined if n is outside of the range of the view
 */
Object StackView_get(StackView view, int n);

/// @}
