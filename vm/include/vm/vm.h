/**
 * @file vm.h
 * @brief Contains the entire state of the Virtual Machine
 * @author Gwilym Kuiper
 */
#pragma once

#include "bytecode.h"
#include "stack.h"
#include "vm_options.h"

/// @addtogroup VM
/// @{

#ifndef MAX_STACK_DEPTH
/**
 * The maximum call stack depth
 */
#define MAX_STACK_DEPTH 1024
#endif

typedef struct GC GC;

/**
 * @brief Stores all of the VM state.
 *
 * The VM is in charge of executing bytecode, be it through the
 * interpreter directly running the bytecode, or with some IR,
 * or compiled code. This is all abstracted through the run
 * function.
 */
typedef struct VM
{
    /** The stack used to store the registers for each fuction call */
    Stack *registerStack;
    /** A pointer to the current bytecode */
    Bytecode *bytecode;
    /** The managed memory object */
    GC *gc;
    /** The globals Dict */
    StackView globalsView;
    /** The call stack */
    int32_t callStack[MAX_STACK_DEPTH];
    /** The current stack depth */
    int stackDepth;

    /** The options */
    VMOptions options;
} VM;

/**
 * @brief Creates a new VM using provided arguments
 * @param options VMOptions for this VM object
 *
 * @return A new VM object
 */
VM *VM_new(VMOptions options);
/**
 * @brief Frees a VM object
 * @param vm The VM to free
 */
void VM_free(VM *vm);

/**
 * @brief Executes the VM until bytecode it runs out of instructions
 * @param vm The VM object
 * @return The Object in position 0 in the stack
 */
Object VM_exec(VM *vm);

/// @}
