/**
 * @file vm_options.h
 * @brief Allows customisation of the VM
 * @author Gwilym Kuiper
 */
#pragma once

/// @addtogroup VM
/// @{

/**
 * @brief The VMOptions is used to customise the execution of the VM
 *
 * Either create this manually, or use VMOptions_new_from_args()
 * which is designed to take the standard (argc, argv) passed in
 * from the command line.
 *
 * The bytecode field has precidence over the filename field.
 * If the bytecode is set, then it is the callers responsibility
 * to free it after the VM has been freed. If filename is set,
 * then the VM will handle that.
 */
typedef struct
{
    /** The file to read the bytecode from */
    const char *filename;
    /** The length of the bytecode */
    int bytecodeLength;
    /** Raw bytecode */
    const char *bytecode;
    /** Verbosity level */
    int verbosity;
} VMOptions;

/**
 * @brief Create a VMOptions from command line arguments
 * @param argc Number of arguments
 * @param argv The arguments
 * @return VMOptions with values depending on the arguments
 */
VMOptions VMOptions_newFromArgs(int argc, char *const *argv);

/// @}
