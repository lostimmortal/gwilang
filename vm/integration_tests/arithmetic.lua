#!/usr/bin/env lua

require('vm.integration_tests.gen_tests')({{
        name = 'Can load shorts',
        expected = '320i',
        code = [[
            loadshort r0 320
        ]]
    }, {
        name = 'Can add integers',
        expected = '6i',
        code = [[
            loadshort r1 3
            loadshort r2 3
            add r0 r1 r2
        ]]
    }, {
        name = 'Can handle negative integers',
        expected = '53i',
        code = [[
            loadshort r1 -7
            loadshort r48 60
            add r0 r48 r1
        ]]
    }, {
        name = 'Can add a constant integer',
        expected = '48i',
        code = [[
            loadshort r1 4
            addk r0 r1 44
        ]]
    }, {
        name = 'Can subtract integers',
        expected = '583i',
        code = [[
            loadshort r1 590
            loadshort r2 7
            sub r0 r1 r2
        ]]
    }, {
        name = 'Can multiply integers',
        expected = '55i',
        code = [[
            loadshort r1 11
            loadshort r0 5
            mul r0 r0 r1
        ]]
    }, {
        name = 'Can divide numbers',
        expected = '5d',
        code = [[
            loadshort r1 10
            loadshort r4 2
            div r0 r1 r4
        ]]
    }, {
        name = 'Can mod numbers',
        expected = '8i',
        code = [[
            loadshort r0 17
            loadshort r1 9
            mod r0 r0 r1
        ]]
    }, {
        name = 'Can find the first prime greater than 1000',
        expected = '1009i',
        code = [[
            loadshort r0 1000
            loadshort r10 0
            :isPrime
            loadshort r1 1
            :loop
            addk r1 r1 1
            mul r2 r1 r1
            gt r5 r2 r0
            jt r5 bigPrime
            mod r4 r0 r1
            eq r5 r4 r10
            jt r5 notPrime
            jmp loop
            :notPrime
            addk r0 r0 1
            jmp isPrime

            :bigPrime
            nop
        ]]
    }
})
