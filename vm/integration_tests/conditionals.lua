#!/usr/bin/env lua

require('vm.integration_tests.gen_tests')({{
        name = 'Can load true',
        expected = 'true',
        code = [[
            loadbool r0 1
        ]]
    }, {
        name = 'Can load false',
        expected = 'false',
        code = [[
            loadbool r0 0
        ]]
    }, {
        name = 'jt will jump if true',
        expected = '3i',
        code = [[
            loadbool r2 1
            loadshort r0 3
            jt r2 skip
            loadshort r0 5
            :skip
            nop
        ]]
    }, {
        name = 'jt will not jump if false',
        expected = '5i',
        code = [[
            loadbool r2 0
            loadshort r0 3
            jt r2 skip
            loadshort r0 5
            :skip
            nop
        ]]
    }, {
        name = 'jt will jump if 0',
        expected = '5i',
        code = [[
            loadshort r2 0
            loadshort r0 5
            jt r2 skip
            loadshort r0 3
            :skip
            nop
        ]]
    }, {
        name = 'jmp will unconditionally jump',
        expected = 'true',
        code = [[
            jmp skip
            loadbool r0 0
            :skip
            loadbool r0 1
            jmp skip2
            :skip2
            nop
        ]]
    }, {
        name = 'lt works with ints',
        expected = 'true',
        code = [[
            loadshort r2 5
            loadshort r4 192
            lt r0 r2 r4
        ]]
    }, {
        name = 'lt works with doubles',
        expected = 'true',
        code = [[
            .constant1 12.2834d
            .constant2 2910.423409d
            loadk r9 constant1
            loadk r39 constant2
            lt r0 r9 r39
        ]]
    }, {
        name = 'lt works with doubles and ints',
        expected = 'true',
        code = [[
            .double 493.39234d
            loadshort r3 3203
            loadk r43 double
            lt r0 r43 r3
        ]]
    }, {
        name = 'lt works with strings',
        expected = 'true',
        code = [[
            $string "i'm a string"
            $otherString "i'm another string"
            loadstr r4 string
            loadstr r5 otherString
            lt r0 r4 r5
        ]]
    }, {
        name = 'lt works with strings the other way',
        expected = 'false',
        code = [[
            $string "i'm a string"
            $otherString "i'm another string"
            loadstr r4 string
            loadstr r5 otherString
            lt r0 r5 r4
        ]]
    }, {
        name = 'lte works',
        expected = 'false',
        code = [[
            loadshort r0 5
            loadshort r2 59
            lte r0 r2 r0
        ]]
    }, {
        name = 'lte works with equality',
        expected = 'true',
        code = [[
            loadshort r0 5
            lte r0 r0 r0
        ]]
    }, {
        name = 'gt works',
        expected = 'true',
        code = [[
            loadshort r4 5
            loadshort r6 8
            gt r0 r6 r4
        ]],
    }, {
        name = 'gt works with equality',
        expected = 'false',
        code = [[
            loadshort r9 5
            gt r0 r9 r9
        ]]
    }, {
        name = 'gte works',
        expected = 'true',
        code = [[
            loadshort r4 594
            loadshort r6 93
            gte r0 r4 r6
        ]]
    }, {
        name = 'gte works with equality',
        expected = 'true',
        code = [[
            loadshort r5 -43
            gte r0 r5 r5
        ]]
    }, {
        name = 'eq works with strings',
        expected = 'true',
        code = [[
            $str1 "string"
            $str2 "string"
            loadstr r7 str1
            loadstr r6 str2
            eq r0 r6 r7
        ]]
    }
})
