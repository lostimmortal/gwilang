#!/usr/bin/env lua

require('vm.integration_tests.gen_tests')({{
        name = 'Can load int constants',
        expected = '320i',
        code = [[
            .constant 320i
            loadk r0 constant
        ]]
    }, {
        name = 'Can load floating point constants',
        expected = '12.5d',
        code = [[
            .constant 12.5d
            loadk r0 constant
        ]]
    }, {
        name = 'Can move registers around',
        expected = '12.5d',
        code = [[
            .constant 12.5d
            loadk r1 constant
            mov r1 r0
        ]]
    }, {
        name = 'Can store and load pointers',
        expected = '2i',
        code = [[
            loadshort r2 29
            storeptr r1 r2
            mov r1 r0
            loadshort r5 2
            loadshort r2 8
            setptr r1 r5
            loadptr r0 r0
        ]]
    }, {
        name = 'GC can handle pointers to themselves',
        expected = '0i',
        code = [[
            .constant 1000i
            loadk r0 constant
            storeptr r20 r0
            setptr r20 r20
            :loop
            $string "hello world, this is a fairly long string which contains a few characters"
            loadstr r1 string
            loadshort r2 -1
            add r0 r0 r2
            loadshort r4 0
            gt r8 r0 r4
            jt r8 loop
        ]]
    }
})
