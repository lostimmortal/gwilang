#!/usr/bin/env lua

require('vm.integration_tests.gen_tests'){{
        name = 'Can create a dict and put stuff in it',
        expected = '55i',
        code = [[
            newdict r1 4
            $myKey "a key for the dict"
            loadstr r2 myKey
            loadshort r3 55
            dictput r1 r2 r3
            dictget r0 r1 r2
        ]]
    }, {
        name = 'Can manage prototypes',
        expected = '55i',
        code = [[
            newdict r1 4
            newdict r2 4
            $key "some string"
            loadstr r4 key
            loadshort r5 55
            dictput r2 r4 r5
            setproto r1 r2
            dictget r0 r1 r4
        ]]
    }, {
        name = 'Can get the prototype of dicts',
        expected = '55i',
        code = [[
            newdict r1 4
            newdict r2 4
            $key "some string"
            loadstr r4 key
            loadshort r5 55
            dictput r2 r4 r5
            setproto r1 r2
            getproto r10 r1
            dictget r0 r10 r4
        ]]
    }, {
        name = 'Can get the prototype of integers',
        expected = '56i',
        code = [[
            loadshort r4 60
            getproto r10 r4
            loadshort r11 56
            dictput r10 r4 r11
            dictget r0 r4 r4
        ]]
    }
}
