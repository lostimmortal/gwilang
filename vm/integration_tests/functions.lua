#!/usr/bin/env lua

require('vm.integration_tests.gen_tests')({{
        name = 'VM will default to the last funcion if there are any',
        expected = '42i',
        code = [[
            ~first 1 0
                ; ensure that any other choice of start place breaks everything
                :loop1
                jmp loop1
            ~second 1 0
                :loop2
                jmp loop2
            ~third 1 0
                loadshort r0 42
        ]],
    }, {
        name = 'Can define and call functions',
        expected = '74i',
        code = [[
            ~addfour 1 0
            addk r60 r0 4
            ret r60

            ~top 0 0
            loadfn r1 addfour
            loadshort r2 70
            call r0 r1 1
        ]]
    }, {
        name = 'Can handle upvalues',
        expected = '74i',
        code = [[
            ~adderfn 1 1
                loadptr r1 r1
                add r0 r0 r1
                ret r0
            ~createadder 1 0
                storeptr r1 r0
                loadfn r0 adderfn
                ret r0
            ~top 0 0
                loadshort r3 4
                loadfn r2 createadder
                call r1 r2 1
                loadshort r2 70
                call r0 r1 1
        ]]
    }, {
        name = 'Can handle mutating upvalues',
        expected = '9i',
        code = [[
            ~iterator 0 1
                loadptr r1 r0
                addk r1 r1 1
                setptr r0 r1
                ret r1
            ~createiterator 0 0
                loadshort r3 0
                storeptr r3 r3
                loadfn r2 iterator
                ret r2
            ~top 0 0
                loadfn r2 createiterator
                call r1 r2 0
                call r0 r1 0
                call r0 r1 0
                call r0 r1 0
                addk r0 r0 6
        ]]
    }
})
