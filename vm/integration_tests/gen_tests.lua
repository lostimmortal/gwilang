local Parser = require('assembler.parser')
local Generation = require('assembler.generation')

local function toCString(data)
    local result = {}
    for i = 1,string.len(data) do
        result[#result + 1] = '\\x' .. string.format('%x', string.byte(data, i))
    end

    return table.concat(result, '')
end

local PREAMBLE = [[
#include <common/test_helper.h>
#include <vm/vm.h>

#include <string.h>

#define ABS(x) ((x) < 0 ? (-x) : (x))
#define ASSERT_CLOSE(actual, expected) \
    ASSERT(ABS(actual - expected) < 10e-15)

VM *vm;
TEARDOWN() {
    VM_free(vm);
}

static VM *createVMWithBytecode(const char *bytecode, int len)
{
    VMOptions o = {
        .bytecode = bytecode,
        .bytecodeLength = len,
    };
    return VM_new(o);
}
]]

local function parseExpected(e)
    if e:sub(-1) == 'i' then
        return ([[
            ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_INT);
            ASSERT_EQUAL(Object_int(o), OUTVAL);
        ]]):gsub('OUTVAL', e:sub(1, -2))
    elseif e == 'true' or e == 'false' then
        return ([[
            ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_BOOLEAN);
            ASSERT_EQUAL(Object_boolean(o), OUTVAL);
        ]]):gsub('OUTVAL', e)
    elseif e:sub(-1) == 'd' then
        return ([[
            ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_NUMBER);
            ASSERT_CLOSE(Object_number(o), OUTVAL);
        ]]):gsub('OUTVAL', e:sub(1, -2))
    elseif e == 'null' then
        return ([[
            ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_NULL);
        ]])
    elseif e:sub(-1) == 's' then
        return ([[
            ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_STRING);
            ASSERT_EQUAL(Object_stringLen(vm->gc, o), OUTLEN);
            ASSERT_EQUAL(memcmp(Object_string(vm->gc, &o), "OUTVAL", OUTLEN), 0);
        ]]):gsub('OUTVAL', e:sub(1, -2))
           :gsub('OUTLEN', e:len() - 1)
    end

    error('Unknown type ' .. e)
end

local function genTest(name, bytecode, expected)
    local outAssertion = parseExpected(expected)
    return (([[
TEST("TESTNAME") {
    vm = createVMWithBytecode("BYTECODE", LENGTH);
    Object o = VM_exec(vm);
    OUT_ASSERTION
}  ]]):gsub('LENGTH', string.len(bytecode))
      :gsub('TESTNAME', name)
      :gsub('BYTECODE', toCString(bytecode))
      :gsub('OUT_ASSERTION', outAssertion))
end

local function lines(text)
    return function ()
        local _, finish, match = text:find("^([^\n]*)\n")
        if finish then
            text = text:sub(finish + 1)
        else
            return nil
        end

        return match
    end
end

return function (tests)
    print(PREAMBLE)
    for _, test in ipairs(tests) do
        local g = Generation()
        local p = Parser(g)
        for line in lines(test.code) do
            p:line(line)
        end

        print(genTest(test.name, g:generate(), test.expected))
    end
end
