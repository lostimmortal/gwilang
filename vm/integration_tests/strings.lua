#!/usr/bin/env lua

require('vm.integration_tests.gen_tests')({{
        name = 'Can load a string',
        expected = 'some strings',
        code = [[
            $myString "some string"
            loadstr r0 myString
        ]]
    }, {
        name = 'Can concatenate strings',
        expected = 'hello, world!s',
        code = [[
            $hello "hello"
            $separator ", "
            $world "world!"

            loadstr r0 hello
            loadstr r1 separator
            concat r0 r0 r1
            loadstr r1 world
            concat r0 r0 r1
        ]]
    }
})
