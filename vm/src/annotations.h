/**
 * @file annotations.h
 * @brief Contains useful annotations for gwilang M6
 * @author Gwilym Kuiper
 */
#pragma once

/**
 * @brief Causes a GC to run
 *
 * A function marked with this annotation will announce
 * that it may cause a GC to run. This should be used for
 * functions that return the new value (for example when
 * inserting an element into a dictionary) and it will
 * always be an error if you don't look at the return type.
 */
#define MAY_GC __attribute__((warn_unused_result))
