#include "bytecode_verification.h"
#include <common/exceptions.h>

#include <stdlib.h>

#define BAIL_IF_ERROR(error)                                                   \
    do {                                                                       \
        if (verified == -1) {                                                  \
            return error;                                                      \
        }                                                                      \
        len -= verified;                                                       \
        bytecode += verified;                                                  \
    } while (0)
static BytecodeVerificationError
Bytecode_verify(Bytecode *b, const char *bytecode, int len)
{
    int verified;
    verified = Bytecode_verifyHeader(bytecode, len);
    BAIL_IF_ERROR(BYTECODE_BAD_HEADER);

    verified =
      Bytecode_verifyNumConstants(bytecode, len, &b->numberOfConstants);
    BAIL_IF_ERROR(BYTECODE_BAD_NUM_OBJECTS);
    b->constants = (Object *) bytecode;

    verified = Bytecode_verifyConstants(bytecode, len, b->numberOfConstants);
    BAIL_IF_ERROR(BYTECODE_BAD_CONSTANT);

    verified = Bytecode_verifyNumConstants(bytecode, len, &b->numberOfStrings);
    BAIL_IF_ERROR(BYTECODE_BAD_NUM_STRINGS);
    b->stringEnds = (uint32_t *) bytecode;
    b->stringData =
      bytecode + 4 * (b->numberOfStrings + b->numberOfStrings % 2);

    verified = Bytecode_verifyStringEnds(bytecode, len, b->numberOfStrings);
    BAIL_IF_ERROR(BYTECODE_BAD_STRING_ENDS);

    verified =
      Bytecode_verifyNumConstants(bytecode, len, &b->numberOfFunctions);
    BAIL_IF_ERROR(BYTECODE_BAD_NUM_FUNCTIONS);
    b->functionData = (FunctionData *) bytecode;
    bytecode += b->numberOfFunctions * 8;
    len -= b->numberOfFunctions * 8;

    if (len % 4 != 0) {
        return BYTECODE_BAD_LENGTH;
    }
    b->opcodeLength = len / 4;
    b->opcodes = (Instruction *) bytecode;

    // TODO Verify the opcodes

    return BYTECODE_NO_ERROR;
}

Bytecode *
Bytecode_new(const char *bytecode, int len, BytecodeVerificationError *error)
{
    Bytecode *ret = malloc(sizeof(Bytecode));
    if ((*error = Bytecode_verify(ret, bytecode, len)) != BYTECODE_NO_ERROR) {
        Bytecode_free(ret);
        return NULL;
    }

    return ret;
}

void
Bytecode_free(Bytecode *b)
{
    free(b);
}

int
Bytecode_numInstructions(Bytecode *bytecode)
{
    return bytecode->opcodeLength;
}

Instruction *
Bytecode_instructions(Bytecode *bytecode)
{
    return bytecode->opcodes;
}

Object
Bytecode_constant(Bytecode *bytecode, int n)
{
    return bytecode->constants[n];
}

Object
Bytecode_string(Bytecode *bytecode, GC *gc, int n)
{
    int startLocation;
    if (n == 0) {
        startLocation = 0;
    } else {
        startLocation = bytecode->stringEnds[n - 1];
    }

    int length = bytecode->stringEnds[n] - startLocation;
    return Object_newString(gc, length, bytecode->stringData + startLocation);
}

int
Bytecode_numFunctions(Bytecode *bytecode)
{
    return bytecode->numberOfFunctions;
}

FunctionData *
Bytecode_function(Bytecode *bytecode, int n)
{
    if (bytecode->numberOfFunctions < n || n < 0) {
        THROW(OUT_OF_RANGE_EXCEPTION,
              "Trying to access a non-existent function");
    }

    return bytecode->functionData + n;
}
