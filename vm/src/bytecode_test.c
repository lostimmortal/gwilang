#include "bytecode_verification.h"
#include <common/exceptions.h>
#include <common/test_helper.h>

#include "gc.h"

#include <stdlib.h>

static Bytecode *bytecode;
static Stack *root;
static StackView view;
static GC *gc;

SETUP()
{
    gc = GC_new(4096);
    root = Stack_new();
    GC_setRoot(gc, root);
    bytecode = malloc(sizeof(Bytecode));

    static uint32_t stringEnds[] = {
        0, 5, 6, 11, 12,
    };
    bytecode->stringEnds = stringEnds;
    bytecode->numberOfStrings = 5;
    bytecode->stringData = "Hello,World!";
    bytecode->numberOfFunctions = 0;
    bytecode->functionData = NULL;

    view = Stack_push(root, 256);
}

TEARDOWN()
{
    Bytecode_free(bytecode);
    GC_free(gc);
    Stack_free(root);
}

TEST("numInstructions returns number of instructions")
{
    bytecode->opcodeLength = 45;
    ASSERT_EQUAL(Bytecode_numInstructions(bytecode), 45);
}

TEST("constant returns nth constant")
{
    Object constants[] = {
        Object_newInt(45), Object_newInt(12093),
    };

    bytecode->constants = constants;
    ASSERT_EQUAL(Object_int(Bytecode_constant(bytecode, 1)), 12093);
}

TEST("Bytecode_string adds first string to GC object")
{
    StackView_put(view, 0, Object_newString(gc, 0, ""));
    Object o = Bytecode_string(bytecode, gc, 0);
    ASSERT(Object_isEqual(gc, o, StackView_get(view, 0)));
}

TEST("Bytecode_string adds second string to GC object")
{
    StackView_put(view, 0, Object_newString(gc, 5, "Hello"));
    Object o = Bytecode_string(bytecode, gc, 1);
    ASSERT(Object_isEqual(gc, o, StackView_get(view, 0)));
}

TEST("Bytecode_string adds third string to GC object")
{
    StackView_put(view, 0, Object_newString(gc, 1, ","));
    Object o = Bytecode_string(bytecode, gc, 2);
    ASSERT(Object_isEqual(gc, o, StackView_get(view, 0)));
}

TEST("Bytecode_string adds last string to GC object")
{
    StackView_put(view, 0, Object_newString(gc, 1, "!"));
    Object o = Bytecode_string(bytecode, gc, 4);
    ASSERT(Object_isEqual(gc, o, StackView_get(view, 0)));
}

TEST("Bytecode_numFunctions returns the number of functions")
{
    bytecode->numberOfFunctions = 4;
    ASSERT_EQUAL(Bytecode_numFunctions(bytecode), 4);
}

TEST("Bytecode_function returns the given function")
{
    bytecode->functionData = malloc(8);
    bytecode->functionData[0] = (FunctionData){
        .numUpvals = 4, .numArgs = 3, .padding__ = 0, .bytecodeLocation = 3
    };

    bytecode->numberOfFunctions = 1;
    ASSERT_EQUAL(Bytecode_function(bytecode, 0)->numUpvals, 4);

    free(bytecode->functionData);
}

TEST_EXPECTING("Bytecode_function throws if function out of range",
               OUT_OF_RANGE_EXCEPTION)
{
    Bytecode_function(bytecode, 12);
}

TEST_EXPECTING("Bytecode_function throws if function is negative",
               OUT_OF_RANGE_EXCEPTION)
{
    Bytecode_function(bytecode, -2);
}
