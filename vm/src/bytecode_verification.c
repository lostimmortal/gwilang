#include "bytecode_verification.h"

int
Bytecode_verifyHeader(const char *bytecode, int len)
{
    if (len < 8) {
        return -1;
    }

    uint32_t header = *(uint32_t *) bytecode;
    if (header != 0x4757494c) {
        return -1;
    }

    uint32_t version = *((uint32_t *) (bytecode + 4));
    if (version != 1) {
        return -1;
    }

    return 8;
}

int
Bytecode_verifyNumConstants(const char *bytecode, int len, uint16_t *constants)
{
    if (len < 8) {
        return -1;
    }

    uint64_t numConstants = *(uint64_t *) bytecode;
    if (numConstants > UINT16_MAX) {
        return -1;
    }

    *constants = numConstants;

    return 8;
}

int
Bytecode_verifyConstants(const char *bytecode, int len, uint16_t numConstants)
{
    if (len < 8 * numConstants) {
        return -1;
    }

    Object *array = (Object *) bytecode;
    for (int i = 0; i < numConstants; i++) {
        switch (Object_type(array[i])) {
            case OBJECT_INT:
            case OBJECT_NUMBER:
                continue;
            default:
                return -1;
        }
    }

    return 8 * numConstants;
}

int
Bytecode_verifyStringEnds(const char *bytecode, int len, uint16_t numStrings)
{
    int effectiveLength = numStrings;
    if (numStrings % 2 == 1) {
        effectiveLength++;
    }

    if (len < 4 * effectiveLength) {
        return -1;
    }

    uint32_t *stringLengths = (uint32_t *) bytecode;
    uint64_t prevLength = 0;
    for (int i = 0; i < numStrings; i++) {
        if (stringLengths[i] < prevLength) {
            return -1;
        }

        prevLength = stringLengths[i];
    }

    // We need to round prevLength up to the nearest 8
    if (prevLength % 8 != 0) {
        prevLength += (8 - prevLength % 8);
    }

    int totalLen = 4 * effectiveLength + prevLength;
    if (len < totalLen) {
        return -1;
    }

    return totalLen;
}
