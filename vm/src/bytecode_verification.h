/**
 * @file bytecode_verification.h
 * @author Gwilym Kuiper
 *
 * This file contains prototypes for the bytecode verification routines.
 * This is mainly present for testing purposes, so that each verification
 * routine can be tested individually rather than having to handle them
 * all in one go.
 */
#pragma once

#include <stdint.h>
#include <vm/bytecode.h>

struct Bytecode
{
    uint16_t numberOfConstants; /**< Number of constants */
    Object *constants;          /**< The constants themselves */
    uint16_t numberOfStrings;   /**< Number of strings */
    uint32_t *stringEnds;       /**< The string ends (used for fast indexing */
    const char *stringData;     /**< The strings themselves */
    uint16_t numberOfFunctions; /**< The number of functions */
    FunctionData *functionData; /**< The data about the functions */
    int opcodeLength;           /**< The number of opcodes */
    Instruction *opcodes;       /**< The opcodes themselves */
};

/**
 * @brief Verifies a header.
 * @param bytecode The bytecode to verify
 * @param len The length of the bytecode
 * @return The number of bytes to advance, or -1 if error
 */
int Bytecode_verifyHeader(const char *bytecode, int len);
/**
 * @brief Verifies the number of constants
 * @param bytecode The bytecode to verify
 * @param len The length of the bytecode
 * @param constants If valid, the number of constants will be stored here
 * @return The number of bytes to advance, or -1 if error
 *
 * Since the number of constants is stored in a 64-bit field but can be
 * at most 2^16, we need to verify that this is the case.
 */
int Bytecode_verifyNumConstants(const char *bytecode, int len,
                                uint16_t *constants);
/**
 * @brief Verifies that bytecode has valid constants stored in it
 * @param bytecode The bytecode to verify
 * @param len The length of the bytecode
 * @param numConstants The number of constants stored here
 * @return The number of bytes to advance, or -1 if error
 */
int Bytecode_verifyConstants(const char *bytecode, int len,
                             uint16_t numConstants);
/**
 * @brief Verifies that the string ends pointers make sense
 * @param bytecode The bytecode to verify
 * @param len The length of the bytecode
 * @param numStrings The number of strings stored here
 * @return The number of bytes to advance, or -1 if error
 */
int Bytecode_verifyStringEnds(const char *bytecode, int len,
                              uint16_t numStrings);
