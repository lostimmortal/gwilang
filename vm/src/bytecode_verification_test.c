#include "bytecode_verification.h"
#include <common/test_helper.h>
#include <vm/object.h>

#include <string.h>

TEST("Checks for correct header length")
{
    ASSERT_EQUAL(Bytecode_verifyHeader("head", 4), -1);
}

TEST("Checks for header contents")
{
    ASSERT_EQUAL(Bytecode_verifyHeader("GWIL\x01\x00\x00\x00", 8), -1);
    ASSERT_EQUAL(Bytecode_verifyHeader("LIWG\x00\x00\x00\x00", 8), -1);
    ASSERT_EQUAL(Bytecode_verifyHeader("LIWG\x01\x00\x00\x00", 8), 8);
}

TEST("Checks for valid length of constants")
{
    uint16_t constants = 0;
    ASSERT_EQUAL(Bytecode_verifyNumConstants("\x00\x00\x00\x00\x01\x00\x00\x00",
                                             7, &constants),
                 -1);
}

TEST("Checks for valid number of constants")
{
    uint16_t constants = 0;
    ASSERT_EQUAL(Bytecode_verifyNumConstants("\x00\x00\x00\x00\x01\x00\x00\x00",
                                             8, &constants),
                 -1);
    ASSERT_EQUAL(Bytecode_verifyNumConstants("\x00\x01\x00\x00\x00\x00\x00\x00",
                                             8, &constants),
                 8);
    ASSERT_EQUAL((int) constants, 0x100);
}

TEST("Checks length of bytecode for constants")
{
    ASSERT_EQUAL(Bytecode_verifyConstants(NULL, 8, 200), -1);
}

TEST("Checks that all elements are INT or NUMBER")
{
    Object arr[] = {
        Object_newInt(4), Object_newNumber(0.4),
    };

    ASSERT_EQUAL(Bytecode_verifyConstants((const char *) arr, 16, 2), 16);
}

TEST("Fails if any object is of the wrong type")
{
    Object arr[] = {
        Object_newInt(4), Object_newBoolean(true),
    };

    ASSERT_EQUAL(Bytecode_verifyConstants((const char *) arr, 16, 2), -1);
}

TEST("Checks length of string array")
{
    ASSERT_EQUAL(Bytecode_verifyStringEnds(NULL, 4, 20), -1);
    // It should be 64-bit aligned
    ASSERT_EQUAL(Bytecode_verifyStringEnds(NULL, 12, 3), -1);
}

TEST("Allows sensible strings")
{
    uint32_t strings[] = { 3, 6, 23 };

    ASSERT_EQUAL(Bytecode_verifyStringEnds((const char *) strings, 16 + 24, 3),
                 16 + 24);
}

TEST("Checks length of bytecode for bad lengths")
{
    uint32_t strings[] = { 3, 6, 23 };

    ASSERT_EQUAL(Bytecode_verifyStringEnds((const char *) strings, 16 + 23, 3),
                 -1);
}

TEST("Checks that all lengths increase")
{
    uint32_t strings[] = { 3, 6, 5 };

    ASSERT_EQUAL(Bytecode_verifyStringEnds((const char *) strings, 10000, 3),
                 -1);
}
