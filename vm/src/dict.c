#include "dict.h"
#include "gc.h"

#include <stdlib.h>

// Set as a fraction to avoid floating point calculation
#define MAX_LOAD(x) (((x) *85) / 100)

static int32_t
max(int32_t a, int32_t b)
{
    return (a > b) ? a : b;
}

/**
 * @brief A Node in the internal Dict array
 *
 * The Node struct is specifically 192 bits in size to prevent unaligned
 * access to the nodes array in Dict. As much of the hash is stored as
 * possible while still trying storing enough information to hold the
 * distance to the initial bucket. This allows us to not have to calculate
 * the hash for every element we lookup.
 */
typedef struct
{
    /** The lower 32 bits of the hash */
    uint32_t hash;
    /** The distance to the initial bucket for this element */
    int32_t dib;
    /** The key for this node */
    Object key;
    /** This node's value */
    Object value;
} Node;

_Static_assert(sizeof(Node) == 24, "Expected node to be 192 bits in size");

/**
 * @brief The key-value store used in Gwilang M6
 *
 * This is a fast key-value storage for Object types. It allows
 * for storing an arbitrary number of key-value pairs and has
 * efficient insert, search, iterate and deleting.
 *
 * Implementation details can be found in @ref dict
 */
typedef struct
{
    /** The size of nodes */
    int32_t size;
    /** The number of elements currently stored */
    int32_t numberOfElements;
    /** The maximum number of jumps from the initial bucket necessary for
     * current storage */
    int32_t maxDIB;
    /** A pointer to the prototype dictionary */
    int32_t proto;

    /** The actual nodes */
    Node nodes[];
} Dict;

static Node
Node_new(Object key, Object value, uint32_t hash)
{
    Node n = {.hash = hash, .dib = 0, .key = key, .value = value };

    return n;
}

static Node
Node_blank(void)
{
    return Node_new(Object_newNull(), Object_newNull(), 0);
}

static Dict *
Dict_dereference(GC *gc, Object dict)
{
    return GC_dereference(gc, Object_pointer(dict));
}

static void
Dict_init(Dict *d, int initialCapacity)
{
    d->size = initialCapacity;
    d->numberOfElements = 0;
    d->maxDIB = 0;
    d->proto = INT32_MIN;

    for (int i = 0; i < initialCapacity; i++) {
        d->nodes[i] = Node_blank();
    }
}

int32_t
Dict_new(GC *gc, int initialCapacity)
{
    int32_t pointer =
      GC_allocDict(gc, sizeof(Dict) + initialCapacity * sizeof(Node));
    Dict_init(GC_dereference(gc, pointer), initialCapacity);

    return pointer;
}

// Gets the location of the key if it exists or -1 if it doesn't
static int
Dict_getLocation(GC *gc, Dict *dict, Object key, uint64_t hash)
{
    for (int32_t dib = 0; dib <= dict->maxDIB; dib++) {
        int32_t location = (hash + dib) % dict->size;
        Node n = dict->nodes[location];
        if (n.hash == (uint32_t) hash && Object_isEqual(gc, n.key, key)) {
            return location;
        } else if (Object_type(n.key) == OBJECT_NULL) {
            break;
        }
    }

    return -1;
}

// Removes a key from the Dict. Will keep following the chain until it finds an
// empty value to attempt to keep search times small. Takes a location to
// save lookups
static void
Dict_remove(Dict *dict, uint64_t location)
{
    while (true) {
        uint64_t nextLocation = (location + 1) % dict->size;
        Node nextNode = dict->nodes[nextLocation];

        // If the next node is empty, then we can blat out the current node.
        if (nextNode.dib == 0 || Object_type(nextNode.key) == OBJECT_NULL) {
            dict->nodes[location] = Node_blank();
            break;
        }

        // We now need to swap the current node with the next one.
        dict->nodes[location] = dict->nodes[nextLocation];
        // The act of doing this reduces the dib for the next location.
        dict->nodes[location].dib--;
        location = nextLocation;
    }

    dict->numberOfElements--;
}

static Object Dict_insert(GC *gc, Object dictObj, Node newNode);

// Resizes and rehashes the entire Dictionary. In future, will do incremental
// rehashing
MAY_GC static Object
Dict_resize(GC *gc, Object dictObj, int newSize)
{
    Dict *dict = Dict_dereference(gc, dictObj);
    int oldSize = dict->size;

    Object newDictObj = Object_newDict(gc, newSize);
    // May get moved during the collection
    dict = Dict_dereference(gc, dictObj);

    for (int i = 0; i < oldSize; i++) {
        Node node = dict->nodes[i];
        node.dib = 0;
        if (Object_type(node.key) != OBJECT_NULL) {
            Dict_insert(gc, newDictObj, node);
        }
    }

    return newDictObj;
}

// Inserts without checking to make sure that the element doesn't exist already
static Object
Dict_insert(GC *gc, Object dictObj, Node newNode)
{
    Dict *dict = Dict_dereference(gc, dictObj);
    // If we need ot resize
    if (MAX_LOAD(dict->size) < dict->numberOfElements) {
        dictObj = Dict_resize(gc, dictObj, dict->size * 2);
        dict = Dict_dereference(gc, dictObj);
    }

    while (Object_type(newNode.key) != OBJECT_NULL) {
        int32_t location = (newNode.hash + newNode.dib) % dict->size;
        Node currentNode = dict->nodes[location];

        // Robin hood, if the old location has a lower DIB then the current
        // value, swap
        if (currentNode.dib <= newNode.dib) {
            dict->nodes[location] = newNode;
            dict->maxDIB = max(newNode.dib, dict->maxDIB);

            newNode = currentNode;
        }

        newNode.dib++;
    }

    dict->numberOfElements++;
    return dictObj;
}

Object
Dict_put(GC *gc, Object dictObj, Object key, Object value)
{
    Dict *dict = Dict_dereference(gc, dictObj);
    uint64_t hash = Object_hash(gc, key);
    // Check if we can replace
    int location = Dict_getLocation(gc, dict, key, hash);
    if (location != -1) {
        // Replace if value isn't null, or delete if it is
        if (Object_type(value) == OBJECT_NULL) {
            Dict_remove(dict, location);
        } else {
            dict->nodes[location].value = value;
        }

        return dictObj;
    }

    return Dict_insert(gc, dictObj, Node_new(key, value, hash));
}

Object
Dict_get(GC *gc, Object dictObj, Object key)
{
    Dict *dict = Dict_dereference(gc, dictObj);
    uint64_t hash = Object_hash(gc, key);
    int location = Dict_getLocation(gc, dict, key, hash);
    if (location == -1) {
        if (dict->proto != INT32_MIN) {
            return Dict_get(gc, Object_newDictFromPointer(dict->proto), key);
        }

        return Object_newNull();
    } else {
        return dict->nodes[location].value;
    }
}

int
Dict_len(GC *gc, Object dict)
{
    return Dict_dereference(gc, dict)->numberOfElements;
}

bool
Dict_iter(GC *gc, Object dictObj, int *i, Object *key, Object *value)
{
    Dict *dict = Dict_dereference(gc, dictObj);
    while (*i < dict->size) {
        Node node = dict->nodes[*i];
        (*i)++;
        if (Object_type(node.key) != OBJECT_NULL) {
            if (key) {
                *key = node.key;
            }
            if (value) {
                *value = node.value;
            }
            return true;
        }
    }

    return false;
}

void
Dict_setProto(GC *gc, Object dictObj, Object proto)
{
    Dict *dict = Dict_dereference(gc, dictObj);
    if (Object_type(proto) == OBJECT_NULL) {
        dict->proto = INT32_MIN;
    } else {
        dict->proto = Object_pointer(proto);
    }
}

Object
Dict_getProto(GC *gc, Object dictObj)
{
    Dict *dict = Dict_dereference(gc, dictObj);
    if (dict->proto == INT32_MIN) {
        return Object_newNull();
    } else {
        return Object_newDictFromPointer(dict->proto);
    }
}

void
Dict_refreshReferences(GC *gc, Object dictObj)
{
    Dict *dict = Dict_dereference(gc, dictObj);

    for (int i = 0; i < dict->size; i++) {
        dict->nodes[i].key = GC_copyObject(gc, dict->nodes[i].key);
        dict->nodes[i].value = GC_copyObject(gc, dict->nodes[i].value);
    }
}
