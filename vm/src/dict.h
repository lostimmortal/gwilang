/**
 * @file dict.h
 * @author Gwilym Kuiper
 * @brief Contains the implementation for a key-value store of Object
 */
#pragma once

#include "annotations.h"
#include <vm/object.h>

typedef struct GC GC;

/**
 * @brief Create a new Dict object
 * @param gc The GC object to create the dictionary in
 * @param initialCapacity The initial capacity of the dictionary
 * @return A new Dict
 */
int32_t Dict_new(GC *gc, int initialCapacity);

/**
 * @brief Insert an element into a Dict
 * @param gc The GC object dict, key and value are defined in
 * @param dict The Dict to insert the element into
 * @param key The key for this element
 * @param value The value
 * @return The new Dict object (may happen if a GC occurs)
 *
 * If value is an Object of type OBJECT_NULL, then the element will
 * instead be removed, and won't be seen when iterating anymore.
 */
MAY_GC Object Dict_put(GC *gc, Object dict, Object key, Object value);
/**
 * @brief Get an element from a Dict
 * @param gc The GC object the Dict is in
 * @param dict The Dict to retrieve the value for
 * @param key The key to search for
 * @return The value of the stored key
 */
Object Dict_get(GC *gc, Object dict, Object key);
/**
 * @brief Gets the number of elements in the Dict
 * @param gc The GC object the dict is in
 * @param dict The Dict to get the number of elements from
 * @return The number of elements
 *
 * This returns the number of elements, not the capacity
 */
int Dict_len(GC *gc, Object dict);

/**
 * @brief Iterate through a Dict
 * @param gc The GC object the dict is in
 * @param dict The dict to iterate through
 * @param i Current iter location
 * @param key Will be filled in with the key
 * @param value Will be filled in with the value
 * @return Whether anything was found
 *
 * The general method for using the iter will be as follows:
 *
 * @code{.c}
 * Object key, value;
 * for (int i = 0; Dict_iter(dict, &i, &key, &value); ) {
 *   // Do something
 * }
 * @endcode
 *
 * The key or value can be NULL if you don't care about their
 * value.
 *
 * Multiple iterator states are supported but modifying the Dict
 * while iterating through it results in undefined behaviour.
 * Also, starting an iteration at a value other than 0 and passing
 * values other than ones which have been given by the Dict_iter
 * function also results in undefined behaviour.
 */
bool Dict_iter(GC *gc, Object dict, int *i, Object *key, Object *value);
/**
 * @brief Set the prototype of dict
 * @param gc The GC object both dict and proto are defined in
 * @param dict The dictionary to set the protoype of
 * @param proto The dictionary to set as the prototype
 */
void Dict_setProto(GC *gc, Object dict, Object proto);
/**
 * @brief Gets the prototype of dict
 * @param gc The GC object both dict and proto are defined in
 * @param dict The dictionary to get the protoype of
 * @return The prototype of dict
 */
Object Dict_getProto(GC *gc, Object dict);

/**
 * @brief Updates all the references for a given Dict
 * @param gc The GC object the Dict is stored in
 * @param dict The Dict object that needs refreshing
 *
 * Needs to happen after a GC to ensure that none of the
 * references go stale.
 */
void Dict_refreshReferences(GC *gc, Object dict);
