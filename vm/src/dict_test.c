#include "dict.h"
#include "gc.h"
#include <common/test_helper.h>
#include <vm/stack.h>

#include <stddef.h>

static Object dict;
static GC *gc;
SETUP()
{
    gc = GC_new(4096);
    dict = Object_newDict(gc, 64);
}

TEARDOWN()
{
    GC_free(gc);
}

TEST("Can get existing elements from a dict")
{
    dict = Dict_put(gc, dict, Object_newInt(4), Object_newInt(5));
    ASSERT_EQUAL(Object_int(Dict_get(gc, dict, Object_newInt(4))), 5);
}

TEST("Can store upto initialCapacity elements")
{
    for (int i = 0; i < 64 * 3; i += 3) {
        dict =
          Dict_put(gc, dict, Object_newInt(i), Object_newBoolean(i % 2 == 0));
    }

    for (int i = 0; i < 64 * 3; i += 3) {
        ASSERT_EQUAL(Object_boolean(Dict_get(gc, dict, Object_newInt(i))),
                     i % 2 == 0);
    }
}

TEST("Can get the length of a dictionary")
{
    ASSERT_EQUAL(Dict_len(gc, dict), 0);

    for (int i = 0; i < 5; i++) {
        dict = Dict_put(gc, dict, Object_newInt(i), Object_newInt(i));
    }

    ASSERT_EQUAL(Dict_len(gc, dict), 5);
}

TEST("Returns NULL if an element is not found")
{
    ASSERT_EQUAL(Object_type(Dict_get(gc, dict, Object_newBoolean(true))),
                 (OBJECT_TYPE) OBJECT_NULL);
}

TEST("Overwrites an old value if key already exists")
{
    for (int i = 0; i < 64; i++) {
        dict = Dict_put(gc, dict, Object_newInt(6), Object_newInt(1));
    }
    dict = Dict_put(gc, dict, Object_newInt(6), Object_newInt(2));

    ASSERT_EQUAL(Object_int(Dict_get(gc, dict, Object_newInt(6))), 2);
    ASSERT_EQUAL(Dict_len(gc, dict), 1);
}

TEST("Can delete entries")
{
    for (int i = 0; i < 64; i++) {
        dict = Dict_put(gc, dict, Object_newInt(i), Object_newInt(i * 2));
    }

    for (int i = 0; i < 64; i += 2) {
        dict = Dict_put(gc, dict, Object_newInt(i + 1), Object_newNull());
    }
    ASSERT_EQUAL(Dict_len(gc, dict), 32);
}

TEST("Can add more than initial capacity")
{
    for (int i = 0; i < 65; i++) {
        dict = Dict_put(gc, dict, Object_newInt(i), Object_newInt(i * 2));
    }

    for (int i = 0; i < 65; i++) {
        ASSERT_EQUAL(Object_int(Dict_get(gc, dict, Object_newInt(i))), i * 2);
    }
}

TEST("Can iterate through a dict")
{
    for (int i = 0; i < 4; i++) {
        dict = Dict_put(gc, dict, Object_newInt(i), Object_newInt(i));
    }

    int maxFound = -1;
    int numFound = 0;

    Object key, value;
    for (int i = 0; Dict_iter(gc, dict, &i, &key, &value);) {
        maxFound = Object_int(key) > maxFound ? Object_int(key) : maxFound;
        numFound++;
    }

    ASSERT_EQUAL(maxFound, 3);
    ASSERT_EQUAL(numFound, 4);
}

TEST("Can pass NULL as the key")
{
    dict = Dict_put(gc, dict, Object_newInt(3), Object_newBoolean(true));

    bool found = false;
    Object value;
    for (int i = 0; Dict_iter(gc, dict, &i, NULL, &value);) {
        found = true;
    }

    ASSERT_EQUAL(found, true);
}

TEST("Can pass NULL as the value")
{
    dict = Dict_put(gc, dict, Object_newInt(3), Object_newBoolean(true));

    bool found = false;
    Object key;
    for (int i = 0; Dict_iter(gc, dict, &i, &key, NULL);) {
        found = true;
    }

    ASSERT_EQUAL(found, true);
}

TEST("Still works after a GC happens")
{
    Stack *stack = Stack_new();
    GC_setRoot(gc, stack);
    StackView view = Stack_push(stack, 1);
    StackView_put(view, 0, dict);

    for (int i = 0; i < 1000; i++) {
        Object str = Object_newString(gc, 13, "Hello, World!");
        dict = Dict_put(gc, StackView_get(view, 0), Object_newInt(i % 10), str);
    }

    Object str = Object_newString(gc, 13, "Hello, World!");
    for (int i = 0; i < 10; i++) {
        ASSERT(Object_isEqual(gc, Dict_get(gc, dict, Object_newInt(i)), str));
    }

    GC_setRoot(gc, NULL);
    Stack_free(stack);
}

TEST("Can give dicts a prototype")
{
    Object dictProto = Object_newDict(gc, 64);
    dictProto =
      Dict_put(gc, dictProto, Object_newInt(3), Object_newBoolean(true));
    Dict_setProto(gc, dict, dictProto);
    ASSERT(Object_isEqual(gc, Dict_get(gc, dict, Object_newInt(3)),
                          Object_newBoolean(true)));
}

TEST("Prototypes don't effect the length")
{
    Object dictProto = Object_newDict(gc, 64);
    dictProto =
      Dict_put(gc, dictProto, Object_newInt(3), Object_newBoolean(true));
    Dict_setProto(gc, dict, dictProto);
    ASSERT_EQUAL(Dict_len(gc, dict), 0);
}

TEST("Setting a prototype to null will remove it")
{
    Object dictProto = Object_newDict(gc, 64);
    dictProto =
      Dict_put(gc, dictProto, Object_newInt(3), Object_newBoolean(true));
    Dict_setProto(gc, dict, dictProto);
    Dict_setProto(gc, dict, Object_newNull());
    ASSERT(Object_isEqual(gc, Dict_get(gc, dict, Object_newInt(3)),
                          Object_newNull()));
}

TEST("Prototypes are safe from GC")
{
    Stack *stack = Stack_new();
    GC_setRoot(gc, stack);
    StackView view = Stack_push(stack, 1);
    StackView_put(view, 0, dict);

    Object dictProto = Object_newDict(gc, 64);
    dictProto =
      Dict_put(gc, dictProto, Object_newInt(3), Object_newBoolean(true));
    Dict_setProto(gc, dict, dictProto);
    for (int i = 0; i < 4096; i++) {
        Object_newString(gc, 13, "Hello, World!");
    }

    ASSERT(
      Object_isEqual(gc, Dict_get(gc, StackView_get(view, 0), Object_newInt(3)),
                     Object_newBoolean(true)));
}

TEST("Getting an unset prototype returns null")
{
    ASSERT_EQUAL(Object_type(Dict_getProto(gc, dict)), OBJECT_NULL);
}

TEST("Getting an set prototype returns the original prototype")
{
    Object dictProto = Object_newDict(gc, 64);
    dictProto =
      Dict_put(gc, dictProto, Object_newInt(3), Object_newBoolean(true));
    Dict_setProto(gc, dict, dictProto);
    ASSERT(Object_isEqual(gc, dictProto, Dict_getProto(gc, dict)));
}
