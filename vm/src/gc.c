#include "dict.h"
#include "gc.h"
#include "strings.h"
#include <common/exceptions.h>

#include <stdlib.h>
#include <string.h>

/**
 * Used by the GC as extended object types to save space
 */
typedef enum {
    /** The size field stores a pointer to where the data has moved to */
    FORWARDING_POINTER = OBJECT_TYPE_LAST,
    /** A string, the bottom is the length of string */
    STRING,
    /** A dictionary, the bottom is the size requested by Dict_new */
    DICT,
    /** A CFunction object */
    CFUNCTION,
    /** An M6 Function object */
    FUNC,
} HEADER_TYPE;

/**
 * The header for all things GC.
 */
typedef struct
{
    /**
     * The header.
     * If it's a pointer, then this is just the object, if it's more then
     * that then the object type will be one of the HEADER_TYPEs
     */
    Object header;
    /** Where the actual entry data is stored */
    union GCEntryData
    {
        char bytes[4];
        int32_t i32[2];
        uint32_t u32[2];
        Object object;
        cfunction cfn;
    } data[];
} GCEntry;

_Static_assert(sizeof(union GCEntryData) == sizeof(uint64_t),
               "GCEntry data is not 64 bits");
_Static_assert(sizeof(GCEntry) == sizeof(uint64_t), "GCObject is not 64 bits");

struct GC
{
    /** We only ever use half of the space in data. Pointers are
     * indexes to this array */
    GCEntry *data;
    /** The current location to insert into */
    int32_t to;
    /** The total size of the memory */
    int32_t size;
    /** The Stack which forms the root of the GC */
    Stack *root;
};

GC *
GC_new(int32_t size)
{
    GC *gc = malloc(sizeof(GC));
    gc->data = malloc(size * 2 * sizeof(GCEntry));
    gc->to = 0;
    gc->size = size;
    gc->root = NULL;

    return gc;
}

void
GC_free(GC *gc)
{
    free(gc->data);
    free(gc);
}

static int32_t
GC_sizeTaken(int32_t length)
{
    return ((int64_t) length + 3) / 4 + 1;
}

/*
 * This logic is non-trivial. We need to see if we are in the
 * first or the second half of the memory space. If in the
 * first, check if adding brings us over the half way point,
 * otherwise we need to check if adding brings us over the
 * maximum.
 */
static bool
GC_haveSpace(GC *gc, int32_t totalSize)
{
    if (gc->to < gc->size) {
        return gc->to + totalSize < gc->size;
    } else {
        return gc->to + totalSize < gc->size * 2;
    }
}

static int32_t
GC_alloc(GC *gc, int32_t length)
{
    /*
     * Total size will be the length rounded up to the nearest 4
     * plus 4 (since all memory is 64-bit aligned). However, we
     * work in units of 64 so we want to divide the length by 4
     * (rounding up) and add 1 (for the GCEntry).
     *
     * Cast to int64_t to avoid overflow errors
     */
    int32_t totalSize = GC_sizeTaken(length);
    if (!GC_haveSpace(gc, totalSize)) {
        GC_collect(gc);

        if (!GC_haveSpace(gc, totalSize)) {
            // Now we really are out of space
            // TODO: Allocate more space for the GC
            THROW(GC_OUT_OF_SPACE_EXCEPTION, "Out of space");
        }
    }

    int32_t result = gc->to;
    gc->to += totalSize;

    return result;
}

static void
GCEntry_newOfTypeAndSize(GCEntry *entry, uint8_t type, int32_t size)
{
    Object o = Object_newOfType(type);
    o.split.bottom = size;
    *entry = (GCEntry){.header = o };
}

int32_t
GC_allocString(GC *gc, int32_t length, const char *data)
{
    int32_t result = GC_allocStringSpace(gc, length);
    memcpy(gc->data[result].data, data, length);

    return result;
}

int32_t
GC_allocStringSpace(GC *gc, int32_t length)
{
    int32_t result = GC_alloc(gc, length);
    GCEntry_newOfTypeAndSize(gc->data + result, STRING, length);

    return result;
}

int32_t
GC_stringLen(GC *gc, int32_t pointer)
{
    return gc->data[pointer].header.split.bottom;
}

void *
GC_dereference(GC *gc, int32_t pointer)
{
    return gc->data[pointer].data;
}

int32_t
GC_allocDict(GC *gc, int32_t size)
{
    int32_t result = GC_alloc(gc, size);
    GCEntry_newOfTypeAndSize(gc->data + result, DICT, size);

    return result;
}

int32_t
GC_allocFunction(GC *gc, uint8_t numUpvalues, uint32_t bytecodeLocation)
{
    int32_t size =
      (numUpvalues + 1) * 4; // The first one is the bytecodelocation
    int32_t result = GC_alloc(gc, size);
    GCEntry_newOfTypeAndSize(gc->data + result, FUNC, size);
    gc->data[result].data[0].u32[0] = bytecodeLocation;

    for (int i = 0; i < numUpvalues; i++) {
        GC_setUpvalue(gc, result, i, INT32_MIN);
    }

    return result;
}

uint32_t
GC_functionBytecodeLocation(GC *gc, int32_t ptr)
{
    return gc->data[ptr].data[0].u32[0];
}

void
GC_setUpvalue(GC *gc, int32_t fn, int n, int32_t ptr)
{
    gc->data[fn].data[(n + 1) / 2].i32[(n + 1) % 2] = ptr;
}

int32_t
GC_getUpvalue(GC *gc, int32_t fn, int n)
{
    return gc->data[fn].data[(n + 1) / 2].i32[(n + 1) % 2];
}

int32_t
GC_allocCFunction(GC *gc, cfunction fn)
{
    int32_t result = GC_alloc(gc, sizeof(cfunction));

    GCEntry_newOfTypeAndSize(gc->data + result, CFUNCTION, sizeof(cfunction));
    gc->data[result].data[0].cfn = fn;

    return result;
}

Object
GC_allocObject(GC *gc, Object obj)
{
    int32_t result = GC_alloc(gc, 0);
    gc->data[result].header = obj;

    return Object_newPointer(result);
}

Object
GC_dereferenceObject(GC *gc, Object obj)
{
    return gc->data[obj.split.bottom].header;
}

void
GC_setPointer(GC *gc, Object ptr, Object obj)
{
    gc->data[ptr.split.bottom].header = obj;
}

void
GC_setRoot(GC *gc, Stack *root)
{
    gc->root = root;
}

static int32_t
GCEntry_getSize(GCEntry *entry)
{
    if (Object_type(entry->header) < OBJECT_TYPE_LAST) {
        return 0;
    } else {
        return entry->header.split.bottom;
    }
}

/*
 * Copies a pointer to the new area, and returns the new
 * pointer for the object. If the pointer has already been
 * moved, just returns the new one.
 */
static int32_t
GC_copy(GC *gc, int32_t pointer)
{
    // First check if we have a forwarding pointer here
    GCEntry entry = gc->data[pointer];
    if (Object_type(entry.header) == FORWARDING_POINTER) {
        return entry.header.split.bottom;
    }

    // We need space for the new entry
    int32_t size = GCEntry_getSize(&entry);
    int32_t newPointer = GC_alloc(gc, size);
    gc->data[newPointer] = entry;
    memcpy(gc->data[newPointer].data, gc->data[pointer].data, size);

    GCEntry_newOfTypeAndSize(gc->data + pointer, FORWARDING_POINTER,
                             newPointer);
    return newPointer;
}

/*
 * We need to ensure that all upvalues are copied and updated correctly
 */
static void
GC_refreshUpvalueReferences(GC *gc, int32_t pointer)
{
    GCEntry entry = gc->data[pointer];
    int size = GCEntry_getSize(&entry);
    int numUpvalues = (size / 4) - 1;
    for (int i = 0; i < numUpvalues; i++) {
        if (GC_getUpvalue(gc, pointer, i) == INT32_MIN) {
            continue;
        }

        GC_setUpvalue(gc, pointer, i,
                      GC_copy(gc, GC_getUpvalue(gc, pointer, i)));
    }
}

Object
GC_copyObject(GC *gc, Object obj)
{
    OBJECT_TYPE type = Object_type(obj);
    if (type == OBJECT_DICTIONARY || type == OBJECT_POINTER ||
        type == OBJECT_FUNCTION) {
        obj.split.bottom = GC_copy(gc, Object_pointer(obj));
    } else if (type == OBJECT_STRING && Strings_length(gc, obj) > 4) {
        obj.split.bottom = GC_copy(gc, Object_pointer(obj));
    }

    return obj;
}

static void
GC_copyRoots(GC *gc)
{
    // Copy all the roots into the new location
    for (int i = 0; i < gc->root->top; i++) {
        gc->root->objects[i] = GC_copyObject(gc, gc->root->objects[i]);
    }
}

void
GC_collect(GC *gc)
{
    // First we need to set the to pointer
    if (gc->to < gc->size) {
        gc->to = gc->size;
    } else {
        gc->to = 0;
    }

    int32_t scanPointer = gc->to;
    if (!gc->root) {
        return;
    }

    GC_copyRoots(gc);

    // Now we go through all the copied elements and follow references
    // to ensure everything is up to date
    while (scanPointer < gc->to) {
        // Get the entry
        GCEntry entry = gc->data[scanPointer];
        // Only Dicts and functions can hold references to other objects
        // If a pointer to a String or other GC object is stored here, that
        // should be copied too.
        uint8_t type = Object_type(entry.header);
        if (type == DICT) {
            Object dict = Object_newDictFromPointer(scanPointer);
            Dict_refreshReferences(gc, dict);
            Dict_setProto(gc, dict, GC_copyObject(gc, Dict_getProto(gc, dict)));
        } else if (type == FUNC) {
            GC_refreshUpvalueReferences(gc, scanPointer);
        } else if (type < OBJECT_TYPE_LAST) {
            gc->data[scanPointer].header = GC_copyObject(gc, entry.header);
        }

        scanPointer += GC_sizeTaken(GCEntry_getSize(&entry));
    }
}
