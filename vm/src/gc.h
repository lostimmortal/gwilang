/**
 * @file gc.h
 * @brief Handles garbage collection
 * @author Gwilym Kuiper
 */
#pragma once

#include <stdint.h>
#include <vm/stack.h>

/**
 * @brief Stores all memory that can't fit in an Object
 *
 * The Garbage Collector is a copy collector. It will, when collecting
 * copy all references from the old memory location to the new one. This
 * is mainly to simplify the implementation. There are many simple
 * improvements that can be made but they are not implemented yet.
 */
typedef struct GC GC;

/**
 * Allocates a new memory object.
 *
 * Currently cannot be dynamically resized so choose the initial size
 * wisely.
 *
 * @param size The initial size
 * @return A new GC object
 */
GC *GC_new(int32_t size);
/**
 * Frees the GC object. Does not do anything clever about freeing what
 * it is storing.
 *
 * @param gc The object to free
 */
void GC_free(GC *gc);

/**
 * Allocates space for a string. Note that this may cause a collection
 *
 * @param gc The GC object to create the string in
 * @param length The length of the string
 * @param data The string itself
 *
 * @return The pointer to the stored string
 */
int32_t GC_allocString(GC *gc, int32_t length, const char *data);
/**
 * Allocates space for a string but does not initialise it. Note that this
 * may cause a GC.
 *
 * @param gc The GC object to create the string in
 * @param len The length of the string
 *
 * @return A pointer to the stored string
 */
int32_t GC_allocStringSpace(GC *gc, int32_t len);
/**
 * Returns the length of the stored string
 *
 * @param gc The GC object the string is stored in
 * @param pointer The pointer returned by GC_allocString
 *
 * @return The length of the string
 */
int32_t GC_stringLen(GC *gc, int32_t pointer);
/**
 * Allocates space for a dictionary. Length is number of bytes to allocate.
 *
 * @param gc The GC object to allocate the dictionary into
 * @param len The size of the space to allocate
 *
 * @return A pointer to the dictionary
 */
int32_t GC_allocDict(GC *gc, int32_t len);
/**
 * Allocates space for a c function. Pass a pointer to the function itself.
 *
 * @param gc The GC object to allocate the function into
 * @param fn The function itself
 *
 * @return A pointer to the function
 */
int32_t GC_allocCFunction(GC *gc, cfunction fn);
/**
 * Allocates space for an M6 function.
 *
 * @param gc The GC object to allocate space for the function
 * @param numUpValues The number of upvalues this function will have
 * @param bytecodeLocation The location in the bytecode the function starts
 * @return A pointer to the allocated space
 */
int32_t GC_allocFunction(GC *gc, uint8_t numUpValues,
                         uint32_t bytecodeLocation);
/**
 * Gets the bytecode location out of a function object
 *
 * @param gc The GC object the function was allocated into
 * @param fn The pointer to the function object
 * @return The bytecode location for that object
 */
uint32_t GC_functionBytecodeLocation(GC *gc, int32_t fn);
/**
 * Sets the nth upvalue for the function
 *
 * @param gc The GC object the function was allocated into
 * @param fn The function itself
 * @param n The index of the upvalue to set
 * @param obj The value to set the nth upvalue to
 */
void GC_setUpvalue(GC *gc, int32_t fn, int n, int32_t obj);
/**
 * Gets the nth upvalue for the function
 *
 * @param gc The GC object the function was allocated into
 * @param fn The function itself
 * @param n The index of the upvalue to get
 * @return The nth upvalue
 */
int32_t GC_getUpvalue(GC *gc, int32_t fn, int n);
/**
 * Allocates space for a given Object
 *
 * @param gc The GC object to allocate the function into
 * @param obj The Object to store
 *
 * @return A pointer to the object that is being stored
 */
Object GC_allocObject(GC *gc, Object obj);

/**
 * Converts a GC pointer to a C pointer which is valid until
 * the next call to GC_alloc*, GC_free or GC_collect
 *
 * @param gc The GC object the pointer is stored in
 * @param pointer The pointer object
 *
 * @return A pointer to the actual data
 */
void *GC_dereference(GC *gc, int32_t pointer);
/**
 * Converts an Object of type OBJECT_POINTER to the Object it points to
 *
 * @param gc The GC object the object itself is stord in
 * @param ptr The Object of type OBJECT_POINTER
 *
 * @return The Object ptr points to
 */
Object GC_dereferenceObject(GC *gc, Object ptr);
/**
 * Sets the value of a pointer
 *
 * @param gc The GC object the OBJECT_POINTER is stored in
 * @param ptr The pointer to set
 * @param obj The object to set ptr to
 */
void GC_setPointer(GC *gc, Object ptr, Object obj);

/**
 * @brief Sets the root of the Collector.
 *
 * For a garbage collector to work, it needs to know what
 * it can't collect. This provides a Stack which stores everything
 * the GC isn't allowed to collect. When collection happens, it
 * will follow each element in the root and not collect them or
 * anything that any such element refers to.
 *
 * @param gc The GC object to set the root for
 * @param root The new root object (will replace any old one)
 */
void GC_setRoot(GC *gc, Stack *root);
/**
 * Runs a collection. A single collection will keep all pointers
 * valid, (read only) but 2 collections don't hold such a guarantee
 * anymore. Only elements with a path to the root will definitely still
 * be reachable. Will modify objects found in root and other
 * collections so ensure to always revalidate non-trivial Object
 * values whenever a collection has run.
 *
 * Collection is based on Cheney's Algorithm as found on wikipedia.
 *
 * @param gc The GC object to collect garbage for
 */
void GC_collect(GC *gc);

/**
 * During a collection, objects may need copying. Only use this
 * function if you know what you're doing.
 *
 * @param gc The GC object the Object is stored in
 * @param obj The object to copy
 * @return The new object
 */
Object GC_copyObject(GC *gc, Object obj);
