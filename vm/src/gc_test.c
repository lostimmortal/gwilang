#include "gc.h"
#include <common/exceptions.h>
#include <common/test_helper.h>

#include <string.h>

#define GC_SIZE 2048

static Stack *roots;
static GC *gc;

static bool testFuncCalled;
SETUP()
{
    roots = Stack_new();
    gc = GC_new(GC_SIZE);

    GC_setRoot(gc, roots);
    testFuncCalled = false;
}

TEARDOWN()
{
    GC_free(gc);
    Stack_free(roots);
}

TEST("can allocate a string")
{
    ASSERT_EQUAL(GC_allocString(gc, 3, "HI!"), 0);
}

TEST_EXPECTING("cannot allocate a large string", GC_OUT_OF_SPACE_EXCEPTION)
{
    GC_allocString(gc, 2048 * 4 + 1, "");
}

TEST_EXPECTING("cannot allocate an extremely large string",
               GC_OUT_OF_SPACE_EXCEPTION)
{
    GC_allocString(gc, INT32_MAX, "");
}

TEST("can allocate more than 1 string")
{
    ASSERT_EQUAL(GC_allocString(gc, 3, "HI!"), 0);
    ASSERT_EQUAL(GC_allocString(gc, 4, "HI!"), 2);
    ASSERT_EQUAL(GC_allocString(gc, 3, "HI!"), 4);
    ASSERT_EQUAL(GC_allocString(gc, 3, "HI!"), 6);

    ASSERT_EQUAL(strcmp(GC_dereference(gc, 2), "HI!"), 0);
}

TEST("can get the length of an allocated string")
{
    int32_t stringPtr = GC_allocString(gc, 3, "Hello, World!");
    ASSERT_EQUAL(GC_stringLen(gc, stringPtr), 3);
}

TEST("can dereference the pointer to the string")
{
    int32_t ptr = GC_allocString(gc, 6, "Hello!");
    ASSERT_EQUAL(strncmp("Hello!", GC_dereference(gc, ptr), 6), 0);
}

TEST("can store strings with null bytes in them")
{
    int32_t ptr = GC_allocString(gc, 6, "H\0llo!");
    ASSERT_EQUAL(memcmp("H\0llo!", GC_dereference(gc, ptr), 6), 0);
}

TEST("Can allocate objects")
{
    Object ptr = GC_allocObject(gc, Object_newInt(4));
    ASSERT_EQUAL(Object_type(ptr), (OBJECT_TYPE) OBJECT_POINTER);
}

TEST("Can maintain pointers in the stack")
{
    StackView view = Stack_push(roots, 1);
    StackView_put(view, 0, GC_allocObject(gc, Object_newInt(4)));
    GC_collect(gc);
    Object o = StackView_get(view, 0);
    ASSERT_EQUAL(Object_int(GC_dereferenceObject(gc, o)), 4);
}

TEST("Can maintain pointers in the stack")
{
    StackView view = Stack_push(roots, 1);
    StackView_put(view, 0, GC_allocObject(gc, Object_newInt(4)));
    GC_setPointer(gc, StackView_get(view, 0), Object_newInt(3924));
    Object o = StackView_get(view, 0);
    ASSERT_EQUAL(Object_int(GC_dereferenceObject(gc, o)), 3924);
}

static Object
testFunction(Context *ctx, int n)
{
    (void) ctx;
    (void) n;
    testFuncCalled = true;
    return Object_newInt(5);
}

TEST("Can allocate functions")
{
    int32_t ptr = GC_allocCFunction(gc, &testFunction);
    ASSERT_EQUAL(*(void **) GC_dereference(gc, ptr), &testFunction);
    cfunction *fn = GC_dereference(gc, ptr);
    ASSERT_EQUAL(Object_int((*fn)(NULL, 0)), 5);
    ASSERT(testFuncCalled);
}

TEST("Can handle GC of functions")
{
    StackView view = Stack_push(roots, 1);
    StackView_put(view, 0, Object_newCFunction(gc, &testFunction));
    GC_collect(gc);
    Object_call(gc, StackView_get(view, 0), NULL, 2);
    ASSERT(testFuncCalled);
}

TEST("copies strings to new half")
{
    StackView view = Stack_push(roots, 1);
    StackView_put(view, 0, Object_newString(gc, 5, "Hello!"));
    GC_collect(gc);
    Object newString = StackView_get(view, 0);
    ASSERT_EQUAL(Object_pointer(newString), GC_SIZE);
    ASSERT_EQUAL(memcmp("Hello!", Object_string(gc, &newString), 5), 0);
}

TEST("copies dictionary along with its elements")
{
    StackView view = Stack_push(roots, 1);

    Object dict = Object_newDict(gc, 5);
    StackView_put(view, 0, dict);
    Object value = Object_newString(gc, 6, "Hello!");
    dict = StackView_get(view, 0);
    Object key = Object_newInt(6);
    dict = Object_dictPut(gc, dict, key, value);
    GC_collect(gc);

    dict = StackView_get(view, 0);
    Object newValue = Object_dictGet(gc, dict, key);
    ASSERT(Object_pointer(newValue) > GC_SIZE);
    ASSERT_EQUAL(memcmp("Hello!", Object_string(gc, &newValue), 6), 0);
}

TEST("objects pointed to twice don't get copied twice")
{
    StackView view = Stack_push(roots, 2);

    Object str = Object_newString(gc, 6, "string");
    StackView_put(view, 1, str);
    Object dict = Object_newDict(gc, 5);
    StackView_put(view, 0, dict);
    Object key = Object_newInt(4);
    dict = Object_dictPut(gc, dict, key, str);

    GC_collect(gc);

    ASSERT_EQUAL(
      Object_pointer(StackView_get(view, 1)),
      Object_pointer(Object_dictGet(gc, StackView_get(view, 0), key)));
}

TEST("GC_collect can run more than once")
{
    StackView view = Stack_push(roots, 2);

    Object str = Object_newString(gc, 6, "string");
    StackView_put(view, 1, str);
    Object dict = Object_newDict(gc, 5);
    StackView_put(view, 0, dict);
    Object key = Object_newInt(4);
    dict = Object_dictPut(gc, dict, key, str);

    GC_collect(gc);
    GC_collect(gc);
    GC_collect(gc);

    ASSERT_EQUAL(
      Object_pointer(StackView_get(view, 1)),
      Object_pointer(Object_dictGet(gc, StackView_get(view, 0), key)));
}

TEST("GC_collect can run without roots")
{
    GC *myGC = GC_new(GC_SIZE);
    GC_collect(myGC);
    GC_free(myGC);
}

TEST_EXPECTING("GC can run out of space", GC_OUT_OF_SPACE_EXCEPTION)
{
    StackView view = Stack_push(roots, GC_SIZE);

    for (int i = 0; i < GC_SIZE; i++) {
        StackView_put(view, i, Object_newString(gc, 5, "hello"));
    }
}

TEST("Can create functions")
{
    int32_t fn = GC_allocFunction(gc, 1, 1192);
    ASSERT_EQUAL(fn, 0);
    ASSERT_EQUAL(GC_functionBytecodeLocation(gc, fn), 1192u);
}

TEST("Can assign and get upvalues from functions")
{
    Object fn = Object_newFunction(gc, 0, 4, 291);
    Object o1 = Object_newInt(4);
    Object o2 = Object_newInt(9);
    Object o3 = Object_newInt(102);
    Object_setUpvalue(gc, fn, 0, GC_allocObject(gc, o1));
    Object_setUpvalue(gc, fn, 1, GC_allocObject(gc, o2));
    Object_setUpvalue(gc, fn, 3, GC_allocObject(gc, o3));

    ASSERT_EQUAL(
      Object_int(GC_dereferenceObject(gc, Object_getUpvalue(gc, fn, 0))), 4);
    ASSERT_EQUAL(
      Object_int(GC_dereferenceObject(gc, Object_getUpvalue(gc, fn, 1))), 9);
    ASSERT_EQUAL(GC_getUpvalue(gc, Object_pointer(fn), 2), INT32_MIN);
    ASSERT_EQUAL(
      Object_int(GC_dereferenceObject(gc, Object_getUpvalue(gc, fn, 3))), 102);
}

TEST("Upvalues are safe from being GCd")
{
    StackView view = Stack_push(roots, 10);
    GC_allocString(gc, 10, "Hello world!!!!");
    Object fn = Object_newFunction(gc, 3, 5, 1923);
    StackView_put(view, 0, fn);
    Object_setUpvalue(gc, fn, 0,
                      GC_allocObject(gc, Object_newString(gc, 6, "hello")));
    GC_allocString(gc, 10, "Hello world!!!!");
    Object_setUpvalue(gc, fn, 1,
                      GC_allocObject(gc, Object_newString(gc, 6, "world")));

    for (int i = 0; i < GC_SIZE; i++) {
        GC_allocString(gc, 20, "Something that isn't HeLlO WoRlD");
    }

    fn = StackView_get(view, 0);

    Object o1 = GC_dereferenceObject(gc, Object_getUpvalue(gc, fn, 0));
    ASSERT_EQUAL(memcmp(Object_string(gc, &o1), "hello", 5), 0);

    Object o2 = GC_dereferenceObject(gc, Object_getUpvalue(gc, fn, 1));
    ASSERT_EQUAL(memcmp(Object_string(gc, &o2), "world", 5), 0);
}
