#include "hash.h"
#include <common/exceptions.h>

#include <stdbool.h>
#include <stdio.h>

#define C_ROUNDS 2
#define D_ROUNDS 4

// Fairly large optimisation here
#define ISNAN(x) __builtin_isnan(x)
#define ISINF(x) __builtin_isinf(x)
#define ISINF_SIGN(x) __builtin_isinf_sign(x)

#define ROTL(value, n) value = (value << n) | (value >> (64 - n))

static uint64_t
charVecToUInt64(const char *key)
{
    uint64_t result = 0;
    for (int i = 0; i < 8; i++) {
        result |= ((uint64_t) key[i]) << (i * 8);
    }

    return result;
}

uint64_t
hashBuffer(const char *buffer, int len, const char key[16])
{
    uint64_t v0 = charVecToUInt64(&key[0]) ^ 0x736f6d6570736575;
    uint64_t v1 = charVecToUInt64(&key[8]) ^ 0x646f72616e646f6d;
    uint64_t v2 = charVecToUInt64(&key[0]) ^ 0x6c7967656e657261;
    uint64_t v3 = charVecToUInt64(&key[8]) ^ 0x7465646279746573;

#define SIP_ROUNDS(num)                                                        \
    do {                                                                       \
        for (int r = 0; r < (num); r++) {                                      \
            v0 += v1;                                                          \
            v2 += v3;                                                          \
            ROTL(v1, 13);                                                      \
            ROTL(v3, 16);                                                      \
            v1 ^= v0;                                                          \
            v3 ^= v2;                                                          \
            ROTL(v0, 32);                                                      \
            v2 += v1;                                                          \
            v0 += v3;                                                          \
            ROTL(v1, 17);                                                      \
            ROTL(v3, 21);                                                      \
            v1 ^= v2;                                                          \
            v3 ^= v0;                                                          \
            ROTL(v2, 32);                                                      \
        }                                                                      \
    } while (0)

    for (int i = 0; i < len - 8; i += 8) {
        v3 ^= charVecToUInt64(&buffer[i]);
        SIP_ROUNDS(C_ROUNDS);
        v0 ^= charVecToUInt64(&buffer[i]);
    }

    uint64_t remaining = 0;
    for (int i = len % 8; i >= 0; i--) {
        remaining |= (uint64_t) buffer[len - i] << ((7 - i) * 8);
    }
    remaining |= (uint64_t)(len % 256) << 56;

    v3 ^= remaining;
    SIP_ROUNDS(C_ROUNDS);
    v0 ^= remaining;

    v2 ^= 0xff;

    SIP_ROUNDS(D_ROUNDS);

    return v0 ^ v1 ^ v2 ^ v3;
}

uint64_t
hashInt32(int32_t number)
{
    union
    {
        int32_t s;
        uint32_t u;
    } convert;
    convert.s = number;
    uint32_t a = convert.u;

    a = (a ^ 61) ^ (a >> 16);
    a = a + (a << 3);
    a = a ^ (a >> 4);
    a = a * 0x27d4eb2d;
    a = a ^ (a >> 15);

    return a;
}

uint64_t
hashDouble(double d)
{
    if (ISNAN(d)) {
        return 23948203948123410;
    } else if (ISINF(d)) {
        return ISINF_SIGN(d) * 23402938420394820;
        // Handling double really being an integer
    } else if (d - (int) d == 0.0) {
        if (INT32_MIN <= d && d <= INT32_MAX) {
            return hashInt32((int32_t) d);
        }
    }

    union
    {
        uint64_t i;
        double d;
    } convert;
    convert.d = d;

    return hashInt32(convert.i) ^ hashInt32(convert.i >> 32);
}

static char key[16];
static bool keySet;

// Currently UNIX only
static void
getRandomKey(void)
{
    FILE *urandom = fopen("/dev/urandom", "r");
    size_t bytesRead = fread(key, 1, 16, urandom);
    if (bytesRead != 16) {
        THROW(RANDOM_SEEDING_FAILED_EXCEPTION,
              "Reading from /dev/urandom failed");
    }
    fclose(urandom);
    keySet = true;
}

uint64_t
hashString(const char *buffer, int len)
{
    if (!keySet) {
        getRandomKey();
    }

    return hashBuffer(buffer, len, key);
}
