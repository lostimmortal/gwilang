/**
 * @file hash.h
 * @author Gwilym Kuiper
 * @brief Provides siphash implementation along with some helper functions
 */
#pragma once

#include <stdint.h>

/**
 * @brief Hashes a string of length len
 *
 * Creates a random static key which it will use for the rest of the
 * running of this application. This key is not going to be the same
 * accross 2 different runs of gwilang m6.
 *
 * @param buffer The string data
 * @param len The length of the string
 * @return The hash of the string
 */
uint64_t hashString(const char *buffer, int len);
/**
 * @brief Calculates a SipHash for a provided buffer
 * @param buffer The buffer to hash
 * @param len The length of the buffer
 * @param key A random key used to hash the buffer
 * @return A hash value of the provided buffer
 */
uint64_t hashBuffer(const char *buffer, int len, const char key[16]);

/**
 * @brief Calculates a hash of a provided 32 bit number
 * @param number The number to hash
 * @return A hash value for the provided number
 *
 * The resulting value is actually 32 bits casted up. The algorithm
 * used can be found here: http://burtleburtle.net/bob/hash/integer.html
 */
uint64_t hashInt32(int32_t number);

/**
 * @brief Calculates a hash of a double
 * @param d The number to hash
 * @return A hash value of the double provided
 *
 * The number is first deterministically converted into a 32-bit
 * integer which is then passed to hashInt32 to actually
 * hash the number
 *
 * The algorithm is based on the lua one
 */
uint64_t hashDouble(double d);
