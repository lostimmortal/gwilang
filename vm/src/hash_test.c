#include "hash.h"
#include <common/test_helper.h>

#include <float.h>
#include <math.h>

TEST("SipHash Against published value")
{
    const char key[16] = {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    };
    const char *buffer =
      "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e";
    ASSERT_EQUAL(hashBuffer(buffer, 15, key), 0xa129ca6149be45e5);
}

TEST("Double hash should always produce the same value for the same number")
{
    ASSERT_EQUAL(hashDouble(-0.0), hashDouble(0.0));
}

TEST("Double hash should cope with +/- infinity, NaN and produce different "
     "values for these")
{
    uint64_t pInf = hashDouble(INFINITY);
    uint64_t mInf = hashDouble(-INFINITY);
    uint64_t nan = hashDouble(NAN);

    ASSERT(pInf != mInf);
    ASSERT(mInf != nan);
    ASSERT(nan != pInf);
}

TEST("Double hash returns the same value for doubles which are integers and "
     "integers")
{
    ASSERT_EQUAL(hashDouble(55.0), hashInt32(55));
}

TEST("Double hash returns different values for different close doubles")
{
    ASSERT_NOT_EQUAL(hashDouble(55.0000001), hashDouble(55.00000001));
    ASSERT_NOT_EQUAL(hashDouble(55.0000000000001), hashInt32(55));
    ASSERT_NOT_EQUAL(hashDouble(DBL_MIN), hashInt32(0));
}
