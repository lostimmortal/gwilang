#include <stdio.h>

int
main(int argc, char **argv)
{
    if (argc != 2) {
        printf("ERR: Expected 1 argument, got %i\n", argc);
        return 1;
    }

    printf("Starting %s...\n", argv[1]);
}
