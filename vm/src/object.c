#include "dict.h"
#include "gc.h"
#include "hash.h"
#include "strings.h"
#include <common/exceptions.h>

#include <math.h>
#include <string.h>

/**
 * @brief Will allow to easily tell if an Object is a number or not
 */
#define IS_OBJECT_MASK 0x7ff0000000000000U

/**
 * @brief Will be 1 if o is an object, 0 otherwise
 * @param o The object to test
 */
static bool
isObject(Object o)
{
    if ((o.data & IS_OBJECT_MASK) == IS_OBJECT_MASK) {
        return o.fullySplit.type > 0;
    }

    return false;
}

static void
throwIfNotOfType(Object obj, OBJECT_TYPE type, const char *message)
{
    if (Object_type(obj) != type) {
        THROW(BAD_OBJECT_TYPE_EXCEPTION, message);
    }
}

#define TYPE_SHIFT 48
#define OBJECT_OF_TYPE(t)                                                      \
    ((Object)(IS_OBJECT_MASK | ((uint64_t)(t) << TYPE_SHIFT)))

uint8_t
Object_type(Object o)
{
    if (isObject(o)) {
        return o.fullySplit.type;
    }

    return OBJECT_NUMBER;
}

Object
Object_newNumber(double n)
{
    return (Object) n;
}

Object
Object_newOfType(uint8_t type)
{
    return OBJECT_OF_TYPE(type);
}

double
Object_number(Object o)
{
    return o.number;
}

double
Object_toNumber(Object o)
{
    if (Object_type(o) == OBJECT_INT) {
        return (double) Object_int(o);
    } else if (Object_type(o) == OBJECT_NUMBER) {
        return Object_number(o);
    } else {
        THROW(BAD_OBJECT_TYPE_EXCEPTION, "Expected a number");
    }
}

Object
Object_newBoolean(bool b)
{
    return (Object)(OBJECT_OF_TYPE(OBJECT_BOOLEAN).data | !!b);
}

Object
Object_newNull(void)
{
    return OBJECT_OF_TYPE(OBJECT_NULL);
}

Object
Object_newInt(int32_t i)
{
    Object o = OBJECT_OF_TYPE(OBJECT_INT);
    o.split.bottom = i;
    return o;
}

Object
Object_newPointer(int32_t location)
{
    Object o = OBJECT_OF_TYPE(OBJECT_POINTER);
    o.split.bottom = location;
    return o;
}

Object
Object_newString(GC *gc, int32_t len, const char *str)
{
    return Strings_new(gc, str, len);
}

int32_t
Object_stringLen(GC *gc, Object str)
{
    throwIfNotOfType(str, OBJECT_STRING, "Expected a string");
    return Strings_length(gc, str);
}

const char *
Object_string(GC *gc, Object *str)
{
    throwIfNotOfType(*str, OBJECT_STRING, "Expected a string");
    return Strings_dereference(gc, str);
}

Object
Object_stringConcat(GC *gc, Object str1, Object str2)
{
    throwIfNotOfType(str1, OBJECT_STRING, "Cannot concat non strings");
    throwIfNotOfType(str2, OBJECT_STRING, "Cannot concat non strings");

    return Strings_concat(gc, str1, str2);
}

#define GO_TO_64_BIT_FOR_OPERATION(a, b, op)                                   \
    do {                                                                       \
        if (Object_type(a) == OBJECT_INT && Object_type(b) == OBJECT_INT) {    \
            int64_t result =                                                   \
              (int64_t) Object_int(a) op(int64_t) Object_int(b);               \
            if (result > INT32_MAX || result < INT32_MIN) {                    \
                return (Object)(double) result;                                \
            } else {                                                           \
                return Object_newInt(result);                                  \
            }                                                                  \
        } else {                                                               \
            return Object_newNumber(Object_toNumber(a) op Object_toNumber(b)); \
        }                                                                      \
    } while (0)

Object
Object_add(Object a, Object b)
{
    GO_TO_64_BIT_FOR_OPERATION(a, b, +);
}

Object
Object_sub(Object a, Object b)
{
    GO_TO_64_BIT_FOR_OPERATION(a, b, -);
}

Object
Object_mul(Object a, Object b)
{
    GO_TO_64_BIT_FOR_OPERATION(a, b, *);
}

Object
Object_div(Object a, Object b)
{
    return Object_newNumber(Object_toNumber(a) / Object_toNumber(b));
}

Object
Object_mod(Object a, Object b)
{
    if (Object_type(a) == OBJECT_INT && Object_type(b) == OBJECT_INT) {
        return Object_newInt(Object_int(a) % Object_int(b));
    } else {
        double aNum = Object_toNumber(a);
        double bNum = Object_toNumber(b);
        return (Object)(aNum - floor(aNum / bNum) * bNum);
    }
}

Object
Object_newDictFromPointer(int32_t pointer)
{
    Object o = OBJECT_OF_TYPE(OBJECT_DICTIONARY);
    o.split.bottom = pointer;
    return o;
}

Object
Object_newDict(GC *gc, int initialCapacity)
{
    return Object_newDictFromPointer(Dict_new(gc, initialCapacity));
}

Object
Object_dictPut(GC *gc, Object dict, Object key, Object value)
{
    throwIfNotOfType(dict, OBJECT_DICTIONARY, "Expected a dictionary");
    return Dict_put(gc, dict, key, value);
}

Object
Object_dictGet(GC *gc, Object dict, Object key)
{
    throwIfNotOfType(dict, OBJECT_DICTIONARY, "Expected a dictionary");
    return Dict_get(gc, dict, key);
}

Object
Object_dictLen(GC *gc, Object dict)
{
    throwIfNotOfType(dict, OBJECT_DICTIONARY, "Expected a dictionary");
    return Object_newInt(Dict_len(gc, dict));
}

void
Object_dictSetProto(GC *gc, Object dict, Object proto)
{
    throwIfNotOfType(dict, OBJECT_DICTIONARY, "Expected a dictionary");
    if (Object_type(proto) != OBJECT_NULL) {
        throwIfNotOfType(proto, OBJECT_DICTIONARY, "Expected a dictionary");
    }
    Dict_setProto(gc, dict, proto);
}

Object
Object_dictGetProto(GC *gc, Object dict)
{
    throwIfNotOfType(dict, OBJECT_DICTIONARY, "Expected a dictionary");
    return Dict_getProto(gc, dict);
}

Object
Object_newCFunction(GC *gc, cfunction fn)
{
    Object o = OBJECT_OF_TYPE(OBJECT_CFUNCTION);
    o.split.bottom = GC_allocCFunction(gc, fn);
    return o;
}

Object
Object_call(GC *gc, Object o, Context *ctx, int n)
{
    throwIfNotOfType(o, OBJECT_CFUNCTION, "Attempting to call a non-function");
    cfunction *fn = GC_dereference(gc, o.split.bottom);
    return (*fn)(ctx, n);
}

bool
Object_isTruthy(Object o)
{
    if (Object_type(o) == OBJECT_BOOLEAN) {
        return Object_boolean(o);
    }

    return Object_type(o) != OBJECT_NULL;
}

uint64_t
Object_hash(GC *gc, Object o)
{
    switch (Object_type(o)) {
        case OBJECT_NULL:
            return 0; // We don't really care here
        case OBJECT_BOOLEAN:
            // Doesn't matter really provided that they are different
            return Object_boolean(o) ? 394823429083209 : 192323482010223;
        case OBJECT_INT:
            return hashInt32(Object_int(o));
        case OBJECT_NUMBER:
            return hashDouble(Object_number(o));
        case OBJECT_STRING:
            return hashString(Object_string(gc, &o), Object_stringLen(gc, o));
        default:
            break;
    }
    return o.data;
}

bool
Object_isEqual(GC *gc, Object a, Object b)
{
    OBJECT_TYPE type = Object_type(a);
    if (type != Object_type(b)) {
        if ((Object_type(a) == OBJECT_NUMBER && Object_type(b) == OBJECT_INT) ||
            (Object_type(a) == OBJECT_INT && Object_type(b) == OBJECT_NUMBER)) {
            return Object_toNumber(a) == Object_toNumber(b);
        }
        return false;
    }

    switch (type) {
        case OBJECT_STRING:
            if (GC_stringLen(gc, Object_pointer(a)) !=
                GC_stringLen(gc, Object_pointer(b))) {
                return false;
            }
            if (Object_pointer(a) == Object_pointer(b)) {
                return true;
            }

            return memcmp(GC_dereference(gc, Object_pointer(a)),
                          GC_dereference(gc, Object_pointer(b)),
                          GC_stringLen(gc, Object_pointer(a))) == 0;
        case OBJECT_BOOLEAN:
            return Object_boolean(a) == Object_boolean(b);
        case OBJECT_INT:
            return Object_int(a) == Object_int(b);
        case OBJECT_NUMBER:
            return Object_number(a) == Object_number(b);
        case OBJECT_NULL:
            return true;
        case OBJECT_DICTIONARY:
            return Object_pointer(a) == Object_pointer(b);
        default:
            return false;
    }
}

static int
min(int a, int b)
{
    return a < b ? a : b;
}

static int
Object_strcmp(GC *gc, Object a, Object b)
{
    if (Object_type(a) != OBJECT_STRING || Object_type(b) != OBJECT_STRING) {
        THROW(BAD_OBJECT_TYPE_EXCEPTION, "Cannot strcmp non strings");
    }

    int lenA = Object_stringLen(gc, a);
    int lenB = Object_stringLen(gc, b);

    int comparison =
      memcmp(Object_string(gc, &a), Object_string(gc, &b), min(lenA, lenB));

    if (comparison == 0) {
        return lenA - lenB;
    }

    return comparison;
}

static int
Object_numcmp(Object a, Object b)
{
    if ((Object_type(a) != OBJECT_INT && Object_type(a) != OBJECT_NUMBER) ||
        (Object_type(b) != OBJECT_INT && Object_type(b) != OBJECT_NUMBER)) {
        THROW(BAD_OBJECT_TYPE_EXCEPTION, "Cannot numcmp non numbers");
    }

    double cmp = Object_toNumber(a) - Object_toNumber(b);

    return cmp < 0 ? -1 : (cmp > 0 ? 1 : 0);
}

static int
Object_cmp(GC *gc, Object a, Object b)
{
    switch (Object_type(a)) {
        case OBJECT_INT:
        case OBJECT_NUMBER:
            return Object_numcmp(a, b);
        case OBJECT_STRING:
            return Object_strcmp(gc, a, b);
        default:
            THROW(BAD_OBJECT_TYPE_EXCEPTION, "Cannot compare these objects");
    }
}

bool
Object_isLessThan(GC *gc, Object a, Object b)
{
    return Object_cmp(gc, a, b) < 0;
}

bool
Object_isGreaterThan(GC *gc, Object a, Object b)
{
    return Object_cmp(gc, a, b) > 0;
}

bool
Object_isLessThanOrEqualTo(GC *gc, Object a, Object b)
{
    return Object_cmp(gc, a, b) <= 0;
}

bool
Object_isGreaterThanOrEqualTo(GC *gc, Object a, Object b)
{
    return Object_cmp(gc, a, b) >= 0;
}

Object
Object_newFunction(GC *gc, uint8_t numArgs, uint8_t numUpvalues,
                   uint32_t bytecodeLocation)
{
    Object o = Object_newOfType(OBJECT_FUNCTION);
    o.split.bottom = GC_allocFunction(gc, numUpvalues, bytecodeLocation);
    o.fullySplit.extra = (numArgs) | (numUpvalues << 8);
    return o;
}

uint8_t
Object_numUpvalues(Object o)
{
    throwIfNotOfType(o, OBJECT_FUNCTION,
                     "Trying to get the number of upvalues of a non-function");
    return o.fullySplit.extra >> 8;
}

uint8_t
Object_numArguments(Object o)
{
    throwIfNotOfType(o, OBJECT_FUNCTION,
                     "Trying to get the number of arguments of a non-function");
    return o.fullySplit.extra & 0xff;
}

uint32_t
Object_bytecodeLocation(GC *gc, Object o)
{
    throwIfNotOfType(o, OBJECT_FUNCTION,
                     "Trying to get the bytecode location of a non-function");
    return GC_functionBytecodeLocation(gc, o.split.bottom);
}

static void
Object_checkUpvalueRange(Object fn, int n)
{
    if (n < 0 || Object_numUpvalues(fn) < n) {
        THROW(OUT_OF_RANGE_EXCEPTION,
              "Trying to index an out of range upvalue");
    }
}

Object
Object_getUpvalue(GC *gc, Object fn, int n)
{
    Object_checkUpvalueRange(fn, n);
    return Object_newPointer(GC_getUpvalue(gc, fn.split.bottom, n));
}

void
Object_setUpvalue(GC *gc, Object fn, int n, Object obj)
{
    Object_checkUpvalueRange(fn, n);
    throwIfNotOfType(obj, OBJECT_POINTER, "All upvalues must be pointers");
    GC_setUpvalue(gc, fn.split.bottom, n, Object_pointer(obj));
}
