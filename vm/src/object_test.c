#include "gc.h"
#include <common/exceptions.h>
#include <common/test_helper.h>
#include <vm/object.h>

#include <math.h>
#include <stddef.h>
#include <string.h>

static GC *gc;

SETUP()
{
    gc = GC_new(2048);
}

TEARDOWN()
{
    GC_free(gc);
}

TEST("object is 64 bits")
{
    ASSERT_EQUAL(sizeof(Object), (size_t) 8);
}

TEST("inifinity is a number")
{
    ASSERT_EQUAL(Object_type((double) INFINITY), (OBJECT_TYPE) OBJECT_NUMBER);
}

TEST("can create numbers")
{
    Object o = Object_newNumber(0.5);
    ASSERT_EQUAL(Object_number(o), 0.5);
}

TEST("numbers have type OBJECT_NUMBER")
{
    ASSERT_EQUAL(Object_type(0.5), (OBJECT_TYPE) OBJECT_NUMBER);
}

TEST("booleans have type OBJECT_BOOLEAN")
{
    Object t = Object_newBoolean(true);
    Object f = Object_newBoolean(false);

    ASSERT_EQUAL(Object_type(t), (OBJECT_TYPE) OBJECT_BOOLEAN);
    ASSERT_EQUAL(Object_type(f), (OBJECT_TYPE) OBJECT_BOOLEAN);
}

TEST("can retrieve booleans")
{
    Object t = Object_newBoolean(true);
    Object f = Object_newBoolean(false);

    ASSERT_EQUAL(Object_boolean(t), true);
    ASSERT_EQUAL(Object_boolean(f), false);
}

TEST("null has type OBJECT_NULL")
{
    Object o = Object_newNull();
    ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_NULL);
}

TEST("integers have type OBJECT_INT")
{
    Object o = Object_newInt(10);
    ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_INT);
}

TEST("can get the value out of an int")
{
    const int32_t i = -10384;
    Object o = Object_newInt(i);
    ASSERT_EQUAL(Object_int(o), i);
}

TEST("can add two ints and get back an int")
{
    Object o1 = Object_newInt(123);
    Object o2 = Object_newInt(321);
    Object sum = Object_add(o1, o2);
    ASSERT_EQUAL(Object_type(sum), (OBJECT_TYPE) OBJECT_INT);
    ASSERT_EQUAL(Object_int(sum), 123 + 321);
}

TEST("can add two doubles and get back a double")
{
    Object sum = Object_add((Object) 1.4, (Object) 4.3);
    ASSERT_EQUAL(Object_type(sum), (OBJECT_TYPE) OBJECT_NUMBER);
    ASSERT_EQUAL(Object_number(sum), 1.4 + 4.3);
}

TEST("can add a double and and an int and get back a double")
{
    Object sum = Object_add((Object) 1.4, Object_newInt(3));
    ASSERT_EQUAL(Object_type(sum), (OBJECT_TYPE) OBJECT_NUMBER);
    ASSERT_EQUAL(Object_number(sum), 4.4);
}

TEST("handles integer overflow")
{
    Object sum = Object_add(Object_newInt(1 << 30), Object_newInt(1 << 30));
    ASSERT_EQUAL(Object_type(sum), (OBJECT_TYPE) OBJECT_NUMBER);
    ASSERT_EQUAL(Object_number(sum), ((int64_t) 1 << 31));
}

TEST("true is truthy")
{
    ASSERT(Object_isTruthy(Object_newBoolean(true)));
}

TEST("false is not truthy")
{
    ASSERT(!Object_isTruthy(Object_newBoolean(false)));
}

TEST("0 is truthy")
{
    ASSERT(Object_isTruthy(Object_newInt(0)));
}

TEST("null is not truthy")
{
    ASSERT(!Object_isTruthy(Object_newNull()));
}

TEST("can create strings")
{
    Object o = Object_newString(gc, 5, "hello");
    ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_STRING);
}

TEST("can get string length")
{
    Object o = Object_newString(gc, 5, "hello");
    ASSERT_EQUAL(Object_stringLen(gc, o), 5);
}

TEST("can get the string data")
{
    Object o = Object_newString(gc, 6, "hello");
    ASSERT_EQUAL(strcmp(Object_string(gc, &o), "hello"), 0);
}

TEST("can create dict objects")
{
    Object o = Object_newDict(gc, 8);
    ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_DICTIONARY);
}

TEST("can store string keys in dictionaries")
{
    Object o = Object_newDict(gc, 8);
    Object str = Object_newString(gc, 5, "hello");
    Object str2 = Object_newString(gc, 5, "hello");
    o = Object_dictPut(gc, o, str, Object_newInt(50));
    ASSERT_EQUAL(Object_int(Object_dictGet(gc, o, str2)), 50);
}

TEST("can store integer and number keys in dictionaries")
{
    Object o = Object_newDict(gc, 8);
    Object key1 = Object_newInt(32);
    Object key2 = Object_newNumber(32.0);
    o = Object_dictPut(gc, o, key1, Object_newInt(50));
    ASSERT_EQUAL(Object_int(Object_dictGet(gc, o, key2)), 50);
}

TEST("can set and get the prototype of a dict object")
{
    Object dict = Object_newDict(gc, 8);
    Object proto = Object_newDict(gc, 8);
    Object_dictSetProto(gc, dict, proto);
    ASSERT_EQUAL(Object_isEqual(gc, Object_dictGetProto(gc, dict), proto),
                 true);
}

TEST("can hash strings")
{
    Object o = Object_newString(gc, 5, "hello");
    Object p = Object_newString(gc, 5, "hello");

    ASSERT_EQUAL(Object_hash(gc, o), Object_hash(gc, p));
}

TEST("can hash null")
{
    ASSERT_EQUAL(Object_hash(gc, Object_newNull()), (uint64_t) 0);
}

TEST("equality works for strings")
{
    Object o = Object_newString(gc, 5, "hello");
    Object p = Object_newString(gc, 5, "hello");
    Object q = Object_newString(gc, 5, "hell0");
    Object r = Object_newString(gc, 6, "hell00");

    ASSERT_EQUAL(Object_isEqual(gc, o, p), true);
    ASSERT_EQUAL(Object_isEqual(gc, o, q), false);
    ASSERT_EQUAL(Object_isEqual(gc, q, r), false);
}

TEST("null is equal to null")
{
    ASSERT(Object_isEqual(gc, Object_newNull(), Object_newNull()));
}

TEST("equality works for booleans")
{
    ASSERT_EQUAL(
      Object_isEqual(gc, Object_newBoolean(true), Object_newBoolean(true)),
      true);
    ASSERT_EQUAL(
      Object_isEqual(gc, Object_newBoolean(true), Object_newBoolean(false)),
      false);
    ASSERT_EQUAL(
      Object_isEqual(gc, Object_newBoolean(false), Object_newBoolean(false)),
      true);
}

TEST("equality works for integers")
{
    ASSERT_EQUAL(Object_isEqual(gc, Object_newInt(3), Object_newInt(3)), true);
    ASSERT_EQUAL(Object_isEqual(gc, Object_newInt(-3), Object_newInt(3)),
                 false);
}

TEST("equality works for doubles")
{
    ASSERT_EQUAL(
      Object_isEqual(gc, Object_newNumber(1.4), Object_newNumber(1.4)), true);
    ASSERT_EQUAL(
      Object_isEqual(gc, Object_newNumber(5.0), Object_newNumber(6.3)), false);
}

TEST("equality works between doubles and integers")
{
    ASSERT_EQUAL(Object_isEqual(gc, Object_newNumber(1.0), Object_newInt(1)),
                 true);
    ASSERT_EQUAL(Object_isEqual(gc, Object_newInt(1), Object_newNumber(1.0)),
                 true);
}

TEST("strings and ints are not equal")
{
    ASSERT_EQUAL(
      Object_isEqual(gc, Object_newString(gc, 2, "12"), Object_newInt(12)),
      false);
}

TEST("equality works for dicts")
{
    Object o1 = Object_newDict(gc, 64);
    Object o2 = Object_newDict(gc, 64);

    ASSERT_EQUAL(Object_isEqual(gc, o1, o2), false);
    ASSERT_EQUAL(Object_isEqual(gc, o1, o1), true);
}

TEST("equal integers and numbers have the same hash")
{
    Object o = Object_newInt(1);
    Object p = Object_newNumber(1.0);

    ASSERT_EQUAL(Object_hash(gc, o), Object_hash(gc, p));
}

static Object
testFunction(Context *ctx, int n)
{
    (void) ctx;
    (void) n;
    return Object_newInt(54);
}

TEST("can create CFunction objects")
{
    Object o = Object_newCFunction(gc, &testFunction);
    ASSERT_EQUAL(Object_type(o), (OBJECT_TYPE) OBJECT_CFUNCTION);
}

TEST("can call CFunctions")
{
    Object o = Object_newCFunction(gc, &testFunction);
    ASSERT_EQUAL(Object_int(Object_call(gc, o, NULL, 1)), 54);
}

TEST("< works on integers")
{
    Object o1 = Object_newInt(3);
    Object o2 = Object_newInt(6);
    ASSERT_EQUAL(Object_isLessThan(gc, o1, o2), true);
    ASSERT_EQUAL(Object_isLessThan(gc, o2, o1), false);
}

TEST("< works on numbers")
{
    Object o1 = (Object) -3.4;
    Object o2 = (Object) 8.7;
    ASSERT_EQUAL(Object_isLessThan(gc, o1, o2), true);
    ASSERT_EQUAL(Object_isLessThan(gc, o2, o1), false);
}

TEST("< works on integers and numbers")
{
    Object o1 = (Object) -3.4;
    Object o2 = Object_newInt(8);
    ASSERT_EQUAL(Object_isLessThan(gc, o1, o2), true);
    ASSERT_EQUAL(Object_isLessThan(gc, o2, o1), false);
}

TEST("< works with strings")
{
    Object o1 = Object_newString(gc, 3, "abc");
    Object o2 = Object_newString(gc, 3, "de");
    ASSERT_EQUAL(Object_isLessThan(gc, o1, o2), true);
    ASSERT_EQUAL(Object_isLessThan(gc, o2, o1), false);
}

TEST("< works with strings sharing a prefix")
{
    Object o1 = Object_newString(gc, 2, "ab");
    Object o2 = Object_newString(gc, 3, "abc");
    ASSERT_EQUAL(Object_isLessThan(gc, o1, o2), true);
    ASSERT_EQUAL(Object_isLessThan(gc, o2, o1), false);
}

TEST("equal strings are not less than eachother")
{
    Object o1 = Object_newString(gc, 3, "abc");
    Object o2 = Object_newString(gc, 3, "abc");
    ASSERT_EQUAL(Object_isLessThan(gc, o1, o2), false);
}

TEST_EXPECTING("< throws exception if bad types", BAD_OBJECT_TYPE_EXCEPTION)
{
    Object o1 = Object_newString(gc, 2, "ab");
    Object o2 = Object_newInt(8);
    Object_isLessThan(gc, o1, o2);
}

TEST_EXPECTING("< throws exception if bad types 2", BAD_OBJECT_TYPE_EXCEPTION)
{
    Object o1 = Object_newString(gc, 2, "ab");
    Object o2 = Object_newInt(8);
    Object_isLessThan(gc, o2, o1);
}

TEST_EXPECTING("< throws exception if passed null", BAD_OBJECT_TYPE_EXCEPTION)
{
    Object o1 = Object_newNull();
    Object o2 = Object_newNull();
    Object_isLessThan(gc, o1, o2);
}

TEST("<= works")
{
    Object o1 = Object_newInt(44);
    Object o2 = Object_newInt(4293);
    ASSERT(Object_isLessThanOrEqualTo(gc, o1, o2));
    ASSERT(!Object_isLessThanOrEqualTo(gc, o2, o1));
    ASSERT(Object_isLessThanOrEqualTo(gc, o2, o2));
}

TEST("> works")
{
    Object o1 = Object_newInt(44);
    Object o2 = Object_newInt(6);
    ASSERT(Object_isGreaterThan(gc, o1, o2));
    ASSERT(!Object_isGreaterThan(gc, o2, o1));
}

TEST(">= works")
{
    Object o1 = Object_newInt(44);
    Object o2 = Object_newInt(5);
    ASSERT(Object_isGreaterThanOrEqualTo(gc, o1, o2));
    ASSERT(Object_isGreaterThanOrEqualTo(gc, o2, o2));
    ASSERT(!Object_isGreaterThanOrEqualTo(gc, o2, o1));
}

TEST("dictLen gets the number of elements in a dictionary")
{
    Object dict = Object_newDict(gc, 8);
    ASSERT_EQUAL(Object_int(Object_dictLen(gc, dict)), 0);

    dict = Object_dictPut(gc, dict, Object_newInt(1), Object_newInt(2));
    ASSERT_EQUAL(Object_int(Object_dictLen(gc, dict)), 1);
}

TEST("subtraction handles two integers")
{
    Object o1 = Object_newInt(4);
    Object o2 = Object_newInt(40);

    ASSERT_EQUAL(Object_int(Object_sub(o1, o2)), -36);
}

TEST("subtraction converts where necessary")
{
    Object o1 = Object_newInt(INT32_MAX);
    Object o2 = Object_newInt(-1);

    ASSERT_EQUAL(Object_type(Object_sub(o1, o2)), (OBJECT_TYPE) OBJECT_NUMBER);
}

TEST_EXPECTING("subtraction of strings is a problem", BAD_OBJECT_TYPE_EXCEPTION)
{
    Object o1 = Object_newString(gc, 5, "hello");
    Object_sub(o1, o1);
}

TEST("multiplication works")
{
    Object o1 = Object_newInt(12);
    Object o2 = Object_newInt(6);
    ASSERT_EQUAL(Object_int(Object_mul(o1, o2)), 12 * 6);
}

TEST("multiplication works for overflow")
{
    Object o1 = Object_newInt(INT32_MAX);
    ASSERT_EQUAL(Object_number(Object_mul(o1, o1)),
                 ((double) INT32_MAX) * ((double) INT32_MAX));
}

TEST("division works")
{
    Object o1 = Object_newInt(10);
    Object o2 = Object_newInt(5);
    ASSERT_EQUAL(Object_number(Object_div(o1, o2)), 2.0);
}

TEST("modulo works with 2 integers")
{
    Object o1 = Object_newInt(100);
    Object o2 = Object_newInt(3);

    ASSERT_EQUAL(Object_int(Object_mod(o1, o2)), 1);
}

TEST("modulo works with numbers")
{
    Object o1 = Object_newNumber(100.0);
    Object o2 = Object_newNumber(75.0);
    ASSERT_EQUAL(Object_number(Object_mod(o1, o2)), 25.0);
}

TEST("modulo works with numbers 2")
{
    Object o1 = Object_newNumber(100.0);
    Object o2 = Object_newNumber(75.5);
    ASSERT_EQUAL(Object_number(Object_mod(o1, o2)), 24.5);
}

TEST("Object_number converts to a number")
{
    Object o = Object_newNumber(100.0);
    ASSERT_EQUAL(Object_number(o), 100.0);
}

TEST("Object_toNumber will cast ints to numbers")
{
    Object o = Object_newInt(100);
    ASSERT_EQUAL(Object_toNumber(o), 100.0);
}

TEST("Object_toNumber will return numbers")
{
    Object o = Object_newNumber(100.0);
    ASSERT_EQUAL(Object_toNumber(o), 100.0);
}

TEST("Object_pointer returns a new pointer")
{
    Object o = Object_newPointer(12345);
    ASSERT_EQUAL(Object_pointer(o), 12345);
}

TEST("Can create new functions")
{
    Object fn = Object_newFunction(gc, 3, 6, 291327);
    ASSERT_EQUAL(Object_numUpvalues(fn), 6);
    ASSERT_EQUAL(Object_numArguments(fn), 3);
    ASSERT_EQUAL(Object_bytecodeLocation(gc, fn), 291327u);
}

TEST("Can set upvalues")
{
    Object fn = Object_newFunction(gc, 3, 6, 291327);
    Object_setUpvalue(gc, fn, 0, Object_newPointer(2));
    ASSERT_EQUAL(Object_getUpvalue(gc, fn, 0).split.bottom, 2);
}

TEST_EXPECTING("Cannot set a non-pointer type upvalue",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object fn = Object_newFunction(gc, 3, 8, 1192120);
    Object_setUpvalue(gc, fn, 0, Object_newInt(3));
}

TEST_EXPECTING("Throws if setting negative upvalue index",
               OUT_OF_RANGE_EXCEPTION)
{
    Object fn = Object_newFunction(gc, 3, 6, 219);
    Object_setUpvalue(gc, fn, -1, Object_newInt(2));
}

TEST_EXPECTING("Throws if setting a too large upvalue", OUT_OF_RANGE_EXCEPTION)
{
    Object_setUpvalue(gc, Object_newFunction(gc, 3, 6, 12), 24,
                      Object_newNull());
}

TEST_EXPECTING("Throws if getting negative upvalue index",
               OUT_OF_RANGE_EXCEPTION)
{
    Object fn = Object_newFunction(gc, 3, 6, 219);
    Object_getUpvalue(gc, fn, -1);
}

TEST_EXPECTING("Throws if setting a too large upvalue", OUT_OF_RANGE_EXCEPTION)
{
    Object_getUpvalue(gc, Object_newFunction(gc, 3, 6, 12), 24);
}

TEST_EXPECTING("Throws if getting the length of a non-string",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_stringLen(gc, Object_newInt(20));
}

TEST_EXPECTING("Throws if getting the string of a non-string",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object o = Object_newInt(20);
    Object_string(gc, &o);
}

TEST_EXPECTING("Throws if putting an element into a non-dict object",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_dictPut(gc, Object_newInt(8), Object_newInt(392),
                   Object_newInt(102));
}

TEST_EXPECTING("Throws if getting an element from a non-dict object",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_dictGet(gc, Object_newInt(8), Object_newInt(392));
}

TEST_EXPECTING("Throws if getting the dict-length of a non-dict object",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_dictLen(gc, Object_newInt(8));
}

TEST_EXPECTING("Throws if calling a non-cfunction", BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_call(gc, Object_newBoolean(1), NULL, 2);
}

TEST_EXPECTING(
  "Throws if trying to get the number of upvalues of a non-function",
  BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_numUpvalues(Object_newInt(3));
}

TEST_EXPECTING(
  "Throws if trying to get the number of arguments of a non-function",
  BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_numArguments(Object_newInt(3));
}

TEST_EXPECTING(
  "Throws if trying to get the bytecode location of a non-function",
  BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_bytecodeLocation(gc, Object_newInt(3));
}

TEST_EXPECTING("Throws if trying to get an upvalue of a non-function",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_getUpvalue(gc, Object_newInt(3), 39);
}

TEST_EXPECTING("Throws if trying to set an upvalue of a non-function",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_setUpvalue(gc, Object_newInt(3), 39, Object_newPointer(209));
}

TEST_EXPECTING("Throws if trying to set the prototype of a non-dict",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_dictSetProto(gc, Object_newInt(3), Object_newDict(gc, 5));
}

TEST_EXPECTING("Throws if trying to set the prototype of a dict to a non-dict",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_dictSetProto(gc, Object_newDict(gc, 5), Object_newInt(3));
}

TEST("Does not throw if trying to set the prototype of a dict to null")
{
    Object_dictSetProto(gc, Object_newDict(gc, 5), Object_newNull());
}

TEST_EXPECTING("Throws if trying to get the prototype of a non-dict object",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_dictGetProto(gc, Object_newInt(3));
}

TEST("Can concatenate strings")
{
    Object str1 = Object_newString(gc, 3, "boo");
    Object str2 = Object_newString(gc, 5, "hello");

    Object expected = Object_newString(gc, 8, "boohello");
    ASSERT(Object_isEqual(gc, Object_stringConcat(gc, str1, str2), expected));
}

TEST_EXPECTING("Concatenation requires a string as the first argument",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_stringConcat(gc, Object_newInt(3), Object_newString(gc, 5, "hello"));
}

TEST_EXPECTING("Concatenation requires a string as second first argument",
               BAD_OBJECT_TYPE_EXCEPTION)
{
    Object_stringConcat(gc, Object_newString(gc, 5, "hello"), Object_newInt(3));
}
