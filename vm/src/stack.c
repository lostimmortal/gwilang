#include <common/exceptions.h>
#include <vm/stack.h>

#include <stdlib.h>

Stack *
Stack_new(void)
{
    Stack *s = malloc(sizeof(Stack));
    s->size = 0;
    s->top = 0;
    s->objects = NULL;
    s->stackViews = NULL;
    s->depth = 0;

    return s;
}

void
Stack_free(Stack *s)
{
    free(s->objects);
    free(s->stackViews);
    free(s);
}

/*
 * For now, just realloc to cap
 */
static void
Stack_ensureCapacity(Stack *s, int cap)
{
    s->objects = realloc(s->objects, cap * sizeof(Object));
    // Ensure all new values are null
    for (int i = s->size; i < cap; i++) {
        s->objects[i] = Object_newNull();
    }

    s->size = cap;
}

static void
Stack_ensureDepth(Stack *s, int depth)
{
    s->stackViews = realloc(s->stackViews, depth * sizeof(int));
}

StackView
Stack_push(Stack *s, int n)
{
    StackView view = {.size = n, .parent = s, .index = s->top };

    s->top += n;
    Stack_ensureCapacity(s, s->top);
    Stack_ensureDepth(s, ++s->depth);
    s->stackViews[s->depth - 1] = n;

    return view;
}

StackView
Stack_pop(Stack *s)
{
    if (s->depth == 0) {
        THROW(CANNOT_POP_STACK_EXCEPTION, "tried to pop an empty stack");
    }

    s->top -= s->stackViews[--s->depth];
    if (s->depth == 0) {
        return (StackView){};
    } else {
        return (StackView){.parent = s,
                           .index = s->top - s->stackViews[s->depth - 1],
                           .size = s->stackViews[s->depth - 1] };
    }
}

#define STACK_POS_FROM_VIEW(view, n) (view).parent->objects[(view).index + (n)]

void
StackView_put(StackView view, int n, Object o)
{
    if (n < 0 || n >= view.size) {
        THROW(OUT_OF_RANGE_EXCEPTION,
              "Trying to put invalid index in StackView");
    }

    STACK_POS_FROM_VIEW(view, n) = o;
}

Object
StackView_get(StackView view, int n)
{
    if (n < 0 || n >= view.size) {
        THROW(OUT_OF_RANGE_EXCEPTION,
              "Trying to get invalid index in StackView");
    }

    return STACK_POS_FROM_VIEW(view, n);
}
