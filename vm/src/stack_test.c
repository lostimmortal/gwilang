#include <common/exceptions.h>
#include <common/test_helper.h>
#include <vm/stack.h>

#include <stdlib.h>

Stack *s;
SETUP()
{
    s = Stack_new();
}

TEARDOWN()
{
    Stack_free(s);
}

TEST("can create a stack view")
{
    StackView view = Stack_push(s, 5);
    ASSERT_EQUAL(view.size, 5);
    ASSERT_EQUAL(view.index, 0);
    ASSERT_EQUAL(view.parent, s);

    StackView view2 = Stack_push(s, 6);
    ASSERT_EQUAL(view2.size, 6);
    ASSERT_EQUAL(view2.index, 5);
    ASSERT_EQUAL(view2.parent, s);
}

TEST("can pop a stack view")
{
    Stack_push(s, 5);
    Stack_push(s, 6);

    Stack_pop(s);
    Stack_pop(s);
}

TEST_EXPECTING("popping an empty stack throws an exception",
               CANNOT_POP_STACK_EXCEPTION)
{
    Stack_pop(s);
}

TEST("popping returns the previous stack view")
{
    Stack_push(s, 5);
    Stack_push(s, 9);
    StackView view3 = Stack_pop(s);
    ASSERT_EQUAL(view3.index, 0);
    ASSERT_EQUAL(view3.size, 5);
}

TEST("can insert in a stack view")
{
    StackView view = Stack_push(s, 5);

    StackView_put(view, 0, Object_newNumber(0.5));
}

TEST_EXPECTING("inserting into a stack view above range throws exception",
               OUT_OF_RANGE_EXCEPTION)
{
    StackView view = Stack_push(s, 5);
    StackView_put(view, 5, Object_newNumber(0.5));
}

TEST_EXPECTING("inserting into a stack view below range throws exception",
               OUT_OF_RANGE_EXCEPTION)
{
    StackView view = Stack_push(s, 5);
    StackView_put(view, -1, Object_newNumber(0.5));
}

TEST("get get from a stack view")
{
    StackView view = Stack_push(s, 5);
    StackView_put(view, 0, Object_newNumber(0.5));

    ASSERT_EQUAL(Object_number(StackView_get(view, 0)), 0.5);
}

TEST_EXPECTING("getting below stack view range throws invalid stack"
               " view exception",
               OUT_OF_RANGE_EXCEPTION)
{
    StackView view = Stack_push(s, 5);

    Object_type(StackView_get(view, -1));
}

TEST_EXPECTING("getting above stack view range throws invalid stack"
               " view exception",
               OUT_OF_RANGE_EXCEPTION)
{
    StackView view = Stack_push(s, 5);

    Object_type(StackView_get(view, 5));
}

TEST("new stack view elements are initialised to null")
{
    StackView view = Stack_push(s, 5);

    for (int i = 0; i < 5; i++) {
        ASSERT_EQUAL(Object_type(StackView_get(view, i)),
                     (OBJECT_TYPE) OBJECT_NULL);
    }
}
