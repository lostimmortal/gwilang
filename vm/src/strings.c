#include "gc.h"
#include "strings.h"

#include <string.h>

static int32_t
min(int32_t a, int32_t b)
{
    return a < b ? a : b;
}

static Object
Strings_alloc(GC *gc, int32_t len)
{
    Object o = Object_newOfType(OBJECT_STRING);
    o.fullySplit.extra = min(len, UINT16_MAX);

    if (len > 4) {
        o.split.bottom = GC_allocStringSpace(gc, len);
    }

    return o;
}

static char *
Strings_dereferenceMutable(GC *gc, Object *string)
{
    if (Strings_length(gc, *string) > 4) {
        return GC_dereference(gc, string->split.bottom);
    }

    return (char *) &string->split.bottom;
}

Object
Strings_new(GC *gc, const char *str, int32_t len)
{
    Object o = Strings_alloc(gc, len);
    memcpy(Strings_dereferenceMutable(gc, &o), str, len);

    return o;
}

const char *
Strings_dereference(GC *gc, Object *string)
{
    return Strings_dereferenceMutable(gc, string);
}

int32_t
Strings_length(GC *gc, Object string)
{
    int32_t len = string.fullySplit.extra;
    if (len == UINT16_MAX) {
        return GC_stringLen(gc, string.split.bottom);
    } else {
        return len;
    }
}

Object
Strings_concat(GC *gc, Object str1, Object str2)
{
    int32_t len1 = Strings_length(gc, str1);
    int32_t len2 = Strings_length(gc, str2);

    Object ret = Strings_alloc(gc, len1 + len2);
    memcpy(Strings_dereferenceMutable(gc, &ret), Strings_dereference(gc, &str1),
           len1);
    memcpy(Strings_dereferenceMutable(gc, &ret) + len1,
           Strings_dereference(gc, &str2), len2);

    return ret;
}
