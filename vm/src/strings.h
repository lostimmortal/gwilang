/**
 * @file strings.h
 * @brief Handles Strings and manipulations
 * @author Gwilym Kuiper
 */
#pragma once

#include <vm/object.h>

typedef struct GC GC;

/**
 * Create a new string
 *
 * @param gc The GC object to allocate the string into
 * @param str The string to set it to
 * @param len The length of the string
 * @return A new string object
 */
Object Strings_new(GC *gc, const char *str, int32_t len);
/**
 * Dereference the string. Since the dereferenced data can be within
 * the object being passed, a pointer must be passed here. Ensure that
 * the return value of Strings_dereference never out-lives the string object.
 *
 * @param gc The GC object the string is allocated in
 * @param string The string to dereference
 * @return The string data
 */
const char *Strings_dereference(GC *gc, Object *string);
/**
 * Get the length of a string
 *
 * @param gc The GC object the string is allocated in
 * @param string The string to dereference
 * @return The string data
 */
int32_t Strings_length(GC *gc, Object string);

/**
 * Returns the concatenation of two strings.
 *
 * @param gc The GC object the strings are allocated in
 * @param str1 The first string in the concatenation
 * @param str2 The second string in the concatenation
 * @return The concatenation of the two strings
 */
Object Strings_concat(GC *gc, Object str1, Object str2);
