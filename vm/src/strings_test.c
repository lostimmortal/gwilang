#include "gc.h"
#include "strings.h"
#include <common/test_helper.h>

#include <string.h>

static GC *gc;

SETUP()
{
    gc = GC_new(2048);
}

TEARDOWN()
{
    GC_free(gc);
}

TEST("can create short strings without a GC")
{
    Object str = Strings_new(NULL, "hi", 2);
    ASSERT_EQUAL(memcmp(Strings_dereference(NULL, &str), "hi", 2), 0);
    ASSERT_EQUAL(Strings_length(NULL, str), 2);
}

TEST("can create long strings")
{
    Object str = Strings_new(gc, "hello, world", 11);
    ASSERT_EQUAL(Strings_length(gc, str), 11);
    ASSERT_EQUAL(memcmp(Strings_dereference(gc, &str), "hello, world", 11), 0);
}

TEST("can concatenate strings")
{
    Object str1 = Strings_new(gc, "hello", 5);
    Object str2 = Strings_new(gc, ", ", 2);
    Object str3 = Strings_new(gc, "world!", 6);

    Object concat = Strings_concat(gc, str1, str2);
    concat = Strings_concat(gc, concat, str3);

    ASSERT_EQUAL(Strings_length(gc, concat), 13);
    ASSERT_EQUAL(memcmp(Strings_dereference(gc, &concat), "hello, world!", 12),
                 0);
}
