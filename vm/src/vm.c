#include "gc.h"
#include <common/exceptions.h>
#include <vm/context.h>
#include <vm/vm.h>

#include <stdlib.h>
#include <string.h>

VM *
VM_new(VMOptions options)
{
    VM *vm = malloc(sizeof(VM));
    vm->registerStack = Stack_new();
    vm->options = options;

    BytecodeVerificationError error;
    vm->bytecode =
      Bytecode_new(options.bytecode, options.bytecodeLength, &error);
    if (error != BYTECODE_NO_ERROR) {
        free(vm);
        return NULL;
    }
    vm->gc = GC_new(4096);

    // We store the protoypes for all object types except pointers. Types are
    // converted into indicies for this array by the method
    // VM_objectTypeToProtoKey
    vm->globalsView = Stack_push(vm->registerStack, 1 + 6);
    for (int i = 0; i < 7; i++) {
        StackView_put(vm->globalsView, i, Object_newDict(vm->gc, 8));
    }

    memset(vm->callStack, 0, MAX_STACK_DEPTH * sizeof(int32_t));
    vm->stackDepth = 0;

    return vm;
}

void
VM_free(VM *vm)
{
    GC_free(vm->gc);
    Bytecode_free(vm->bytecode);
    Stack_free(vm->registerStack);
    free(vm);
}

static inline int
min(int a, int b)
{
    return a < b ? a : b;
}

static int
VM_callFunction(VM *vm, StackView reg, StackView newView, uint8_t fn,
                uint8_t args, int retPointer)
{
    if (vm->stackDepth++ == MAX_STACK_DEPTH) {
        THROW(CALL_STACK_EXCEEDED_EXCEPTION, "Maximum call stack exceeded");
    }

    vm->callStack[vm->stackDepth - 1] = retPointer;
    Object function = StackView_get(reg, fn);
    int numArgs = Object_numArguments(function);
    int numUpvals = Object_numUpvalues(function);

    // Copy the arguments into the new stack view
    for (int i = 0; i < min(numArgs, args); i++) {
        StackView_put(newView, i, StackView_get(reg, fn + 1 + i));
    }
    // Copy the upvalues into the new stack view
    for (int i = numArgs; i < numArgs + numUpvals; i++) {
        StackView_put(newView, i,
                      Object_getUpvalue(vm->gc, function, i - numArgs));
    }

    return Object_bytecodeLocation(vm->gc, function) - 1;
}

static int
VM_return(VM *vm, StackView reg, Object whatToReturn)
{
    // We need to work out which register to stick this return register into
    Instruction callInstruction =
      Bytecode_instructions(vm->bytecode)[vm->callStack[--vm->stackDepth]];

    uint8_t destReg = callInstruction.A.dest;
    StackView_put(reg, destReg, whatToReturn);

    return vm->callStack[vm->stackDepth];
}

static Object
VM_callCFunction(VM *vm, StackView reg, uint8_t fn, uint8_t args)
{
    Context ctx = {
        .vm = vm, .view = Stack_push(vm->registerStack, args),
    };

    // Copy arguments to the function
    for (int i = 0; i < args; i++) {
        StackView_put(ctx.view, i, StackView_get(reg, fn + i + 1));
    }

    Object ret = Object_call(vm->gc, StackView_get(reg, fn), &ctx, args);
    Stack_pop(vm->registerStack);
    return ret;
}

static int
VM_objectTypeToProtoKey(OBJECT_TYPE type)
{
    switch (type) {
        case OBJECT_NUMBER:
        case OBJECT_INT:
            return 1;
        case OBJECT_NULL:
            return 2;
        case OBJECT_BOOLEAN:
            return 3;
        case OBJECT_STRING:
            return 4;
        case OBJECT_DICTIONARY:
            return -1; // Should use the built in prototype
        case OBJECT_FUNCTION:
        case OBJECT_CFUNCTION:
            return 5;
        default:
            THROW(BAD_OBJECT_TYPE_EXCEPTION,
                  "Cannot get prototype of this object");
    }
}

static Object
VM_getProto(VM *vm, Object key)
{
    OBJECT_TYPE type = Object_type(key);
    if (type == OBJECT_DICTIONARY) {
        return Object_dictGetProto(vm->gc, key);
    } else {
        return StackView_get(vm->globalsView, VM_objectTypeToProtoKey(type));
    }
}

static Object
VM_dictToGetFrom(VM *vm, Object key)
{
    OBJECT_TYPE type = Object_type(key);
    if (type == OBJECT_DICTIONARY) {
        return key;
    } else {
        return StackView_get(vm->globalsView, VM_objectTypeToProtoKey(type));
    }
}

// Currently a big while-switch loop. Will be made more exciting
// in future.
Object
VM_exec(VM *vm)
{
    StackView reg = Stack_push(vm->registerStack, 256);
    int ip = 0;
    int numInstructions = Bytecode_numInstructions(vm->bytecode);
    Instruction *instructions = Bytecode_instructions(vm->bytecode);

    if (Bytecode_numFunctions(vm->bytecode) != 0) {
        ip = Bytecode_function(vm->bytecode,
                               Bytecode_numFunctions(vm->bytecode) - 1)
               ->bytecodeLocation;
    }

    while (ip != numInstructions) {
        Instruction i = instructions[ip];
        switch (i.A.opcode) {
            case NOP: // do nothing, nop
                break;
            case LOADSHORT: // load short
                StackView_put(reg, i.B.dest, Object_newInt(i.B.data.s));
                break;
            case ADD: // add
                StackView_put(reg, i.A.dest,
                              Object_add(StackView_get(reg, i.A.src1),
                                         StackView_get(reg, i.A.src2)));
                break;
            case JMP:
                // Need -1 since we unconditionally add in the loop
                ip += i.B.data.s - 1;
                break;
            case ADDK: // add constant
                StackView_put(reg, i.A.dest,
                              Object_add(StackView_get(reg, i.A.src1),
                                         Object_newInt(i.A.src2)));
                break;
            case LOADBOOL: // load boolean
                StackView_put(reg, i.B.dest, Object_newBoolean(!!i.B.data.s));
                break;
            case JT: // jump if true
                if (Object_isTruthy(StackView_get(reg, i.B.dest))) {
                    ip += i.B.data.s - 1;
                }
                break;
            case LOADK:
                StackView_put(reg, i.B.dest,
                              Bytecode_constant(vm->bytecode, i.B.data.u));
                break;
            case LOADSTR:
                StackView_put(
                  reg, i.B.dest,
                  Bytecode_string(vm->bytecode, vm->gc, i.B.data.u));
                break;
            case NEWDICT:
                StackView_put(reg, i.B.dest,
                              Object_newDict(vm->gc, i.B.data.u));
                break;
            case DICTPUT:
                StackView_put(reg, i.A.dest,
                              Object_dictPut(vm->gc,
                                             StackView_get(reg, i.A.dest),
                                             StackView_get(reg, i.A.src1),
                                             StackView_get(reg, i.A.src2)));
                break;
            case DICTGET:
                StackView_put(
                  reg, i.A.dest,
                  Object_dictGet(
                    vm->gc, VM_dictToGetFrom(vm, StackView_get(reg, i.A.src1)),
                    StackView_get(reg, i.A.src2)));
                break;
            case LOADG:
                StackView_put(reg, i.A.dest,
                              Object_dictGet(vm->gc,
                                             StackView_get(vm->globalsView, 0),
                                             StackView_get(reg, i.A.src1)));
                break;
            case CALL:
                if (Object_type(StackView_get(reg, i.A.src1)) ==
                    OBJECT_CFUNCTION) {
                    StackView_put(
                      reg, i.A.dest,
                      VM_callCFunction(vm, reg, i.A.src1, i.A.src2));
                } else {
                    StackView oldReg = reg;
                    reg = Stack_push(vm->registerStack, 256);
                    ip =
                      VM_callFunction(vm, oldReg, reg, i.A.src1, i.A.src2, ip);
                }
                break;
            case LT:
                StackView_put(reg, i.A.dest,
                              Object_newBoolean(Object_isLessThan(
                                vm->gc, StackView_get(reg, i.A.src1),
                                StackView_get(reg, i.A.src2))));
                break;
            case LTE:
                StackView_put(reg, i.A.dest,
                              Object_newBoolean(Object_isLessThanOrEqualTo(
                                vm->gc, StackView_get(reg, i.A.src1),
                                StackView_get(reg, i.A.src2))));
                break;
            case GT:
                StackView_put(reg, i.A.dest,
                              Object_newBoolean(Object_isGreaterThan(
                                vm->gc, StackView_get(reg, i.A.src1),
                                StackView_get(reg, i.A.src2))));
                break;
            case GTE:
                StackView_put(reg, i.A.dest,
                              Object_newBoolean(Object_isGreaterThanOrEqualTo(
                                vm->gc, StackView_get(reg, i.A.src1),
                                StackView_get(reg, i.A.src2))));
                break;
            case EQ:
                StackView_put(reg, i.A.dest,
                              Object_newBoolean(Object_isEqual(
                                vm->gc, StackView_get(reg, i.A.src1),
                                StackView_get(reg, i.A.src2))));
                break;
            case SUB:
                StackView_put(reg, i.A.dest,
                              Object_sub(StackView_get(reg, i.A.src1),
                                         StackView_get(reg, i.A.src2)));
                break;
            case MOV:
                StackView_put(reg, i.A.src1, StackView_get(reg, i.A.dest));
                break;
            case MUL:
                StackView_put(reg, i.A.dest,
                              Object_mul(StackView_get(reg, i.A.src1),
                                         StackView_get(reg, i.A.src2)));
                break;
            case DIV:
                StackView_put(reg, i.A.dest,
                              Object_div(StackView_get(reg, i.A.src1),
                                         StackView_get(reg, i.A.src2)));
                break;
            case MOD:
                StackView_put(reg, i.A.dest,
                              Object_mod(StackView_get(reg, i.A.src1),
                                         StackView_get(reg, i.A.src2)));
                break;
            case STOREPTR:
                StackView_put(
                  reg, i.A.dest,
                  GC_allocObject(vm->gc, StackView_get(reg, i.A.src1)));
                break;
            case LOADPTR:
                StackView_put(
                  reg, i.A.dest,
                  GC_dereferenceObject(vm->gc, StackView_get(reg, i.A.src1)));
                break;
            case SETPTR:
                GC_setPointer(vm->gc, StackView_get(reg, i.A.dest),
                              StackView_get(reg, i.A.src1));
                break;
            case LOADFN:;
                FunctionData *fnData =
                  Bytecode_function(vm->bytecode, i.B.data.u);
                Object fn =
                  Object_newFunction(vm->gc, fnData->numArgs, fnData->numUpvals,
                                     fnData->bytecodeLocation);
                StackView_put(reg, i.B.dest, fn);
                for (int j = 0; j < fnData->numUpvals; j++) {
                    Object_setUpvalue(vm->gc, fn, j,
                                      StackView_get(reg, i.B.dest + j + 1));
                }
                break;
            case RET:;
                Object whatToReturn = StackView_get(reg, i.A.dest);
                reg = Stack_pop(vm->registerStack);
                ip = VM_return(vm, reg, whatToReturn);
                break;
            case SETPROTO:
                Object_dictSetProto(vm->gc, StackView_get(reg, i.A.dest),
                                    StackView_get(reg, i.A.src1));
                break;
            case GETPROTO:
                StackView_put(reg, i.A.dest,
                              VM_getProto(vm, StackView_get(reg, i.A.src1)));
                break;
            case CONCAT:
                StackView_put(
                  reg, i.A.dest,
                  Object_stringConcat(vm->gc, StackView_get(reg, i.A.src1),
                                      StackView_get(reg, i.A.src2)));
                break;
        }

        ip++;
    }

    return StackView_get(reg, 0);
}
