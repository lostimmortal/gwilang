#include <vm/vm_options.h>

#include <unistd.h>

VMOptions
VMOptions_newFromArgs(int argc, char *const *argv)
{
    VMOptions o = {
        .filename = NULL, .bytecode = NULL, .verbosity = 0,
    };

    char flag;
    while ((flag = getopt(argc, argv, "v")) != -1) {
        switch (flag) {
            case 'v':
                o.verbosity++;
                break;
        }
    }

    if (optind < argc) {
        o.filename = argv[optind];
    }

    return o;
}
