#include <common/test_helper.h>
#include <vm/vm_options.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static VMOptions
optionsWithArgs(int n, const char *const argv[n])
{
    char *argvCopy[n];
    for (int i = 0; i < n; i++) {
        argvCopy[i] = strdup(argv[i]);
    }
    VMOptions o = VMOptions_newFromArgs(n, argvCopy);
    optind = 1;

    for (int i = 0; i < n; i++) {
        free(argvCopy[i]);
    }

    return o;
}

TEST("Argument parsing handles verbose flag")
{
    const char *const argv[] = { "./m6", "-v", "-v", "file.m6" };
    VMOptions o = optionsWithArgs(4, argv);
    ASSERT_EQUAL(o.verbosity, 2);
}

TEST("Argument parsing handles gets the correct file")
{
    const char *const argv[] = { "./m6", "-v", "-v", "file.m6" };
    VMOptions o = optionsWithArgs(4, argv);
    // Unfortunately, the helper function frees the reference
    // to the filename we collect...
    ASSERT_NOT_EQUAL(o.filename, NULL);
}
